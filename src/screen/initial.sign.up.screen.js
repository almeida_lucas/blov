/**
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Navigation } from 'react-native-navigation';

import Button from '../component/button.component';
import connect from 'react-redux/es/connect/connect';

type Props = {};

class InitialSignUp extends Component<Props> {

  static get options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    };
  }

  _nextSignUpScreen = () => Navigation.push(this.props.componentId, {
    component: {
      id: 'FirstSignUp',
      name: 'FirstSignUp',
      passProps: {},
    },
  });

  render() {
    const {user} = this.props;

    return (
      <View style={styles.container}>
        <Image
          style={{width: 90, height: 90}}
          source={require('../resource/logo.png')}
        />

        <View style={{alignItems: 'center'}}>
          <Text style={{fontSize: 40, color: '#fff', textAlign: 'center'}}>Olá <Text style={{fontWeight: 'bold'}}>{user ? user.name : '-'}</Text>,</Text>
          <Text style={{fontSize: 23, color: '#fff'}}>seja bem vindo</Text>
        </View>
        <Text style={{fontSize: 20, color: '#fff', textAlign: 'center'}}>
          Para começarmos, gostaríamos de saber um pouco mais sobre você, que tal?
        </Text>

        <Button
          onPress={this._nextSignUpScreen}
          text='Começar agora'
        />

      </View>
    );
  }
}

const mapStateToProps = ({user}) => {
  return {
    user,
  };
};

export default connect(mapStateToProps, null)(InitialSignUp);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#7A1043',
    padding: 15,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
