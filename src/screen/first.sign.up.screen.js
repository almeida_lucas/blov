/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
  Platform,
  Image,
  TouchableOpacity,
  Modal,
  Alert,
} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {
  Picker,
  MaskedInput,//TODO formatar mascara com esse componente
} from 'react-native-ui-lib';
import {TextInputMask} from 'react-native-masked-text';
import dropdown from '../resource/chevronDown.png';
import ProfilePicture from '../component/profile.picture.component';
import Button from '../component/button.component';
import connect from 'react-redux/es/connect/connect';
import {fetchCities} from '../action/cities.action';
import {
  deletePicture,
  updateUserProp,
} from '../action/user.action';
import LoaderScreen from '../component/loading.screen.component';
import Icon from 'react-native-vector-icons/EvilIcons';
import Header from '../component/header.component';
import showMessage from '../constant/native.message';

import {DELETE_PICTURE_SUCCESS} from '../constant';

const optionsGender = [
  {
    label: 'Masculino',
    value: 'M',
  },
  {
    label: 'Feminino',
    value: 'F',
  },
];
const optionsState = [
  {
    label: '',
    value: '',
  },
  {
    label: 'AC',
    value: 'AC',
  },
  {
    label: 'AL',
    value: 'AL',
  },
  {
    label: 'AP',
    value: 'AP',
  },
  {
    label: 'AM',
    value: 'AM',
  },
  {
    label: 'BA',
    value: 'BA',
  },
  {
    label: 'CE',
    value: 'CE',
  },
  {
    label: 'DF',
    value: 'DF',
  },
  {
    label: 'ES',
    value: 'ES',
  },
  {
    label: 'GO',
    value: 'GO',
  },
  {
    label: 'MA',
    value: 'MA',
  },
  {
    label: 'MT',
    value: 'MT',
  },
  {
    label: 'MS',
    value: 'MS',
  },
  {
    label: 'MG',
    value: 'MG',
  },
  {
    label: 'PA',
    value: 'PA',
  },
  {
    label: 'PB',
    value: 'PB',
  },
  {
    label: 'PR',
    value: 'PR',
  },
  {
    label: 'PE',
    value: 'PE',
  },
  {
    label: 'PI',
    value: 'PI',
  },
  {
    label: 'RJ',
    value: 'RJ',
  },
  {
    label: 'RN',
    value: 'RN',
  },
  {
    label: 'RS',
    value: 'RS',
  },
  {
    label: 'RO',
    value: 'RO',
  },
  {
    label: 'RR',
    value: 'RR',
  },
  {
    label: 'SC',
    value: 'SC',
  },
  {
    label: 'SP',
    value: 'SP',
  },
  {
    label: 'SE',
    value: 'SE',
  },
  {
    label: 'TO',
    value: 'TO',
  },

  /*Acre
   Alagoas
   Amapá
   Amazonas
   Bahia
   Ceará
   Distrito Federal
   Espírito Santo
   Goiás
   Maranhão
   Mato Grosso
   Mato Grosso do Sul
   Minas Gerais
   Pará
   Paraíba
   Paraná
   Pernambuco
   Piauí
   Rio de Janeiro
   Rio Grande do Norte
   Rio Grande do Sul
   Rondônia
   Roraima
   Santa Catarina
   São Paulo
   Sergipe
   Tocantins*/
];

type Props = {};

class FirstSignUp extends Component<Props> {

  static get options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    };
  }

  constructor(props) {
    super(props);
    const {user, isEditProfile} = props;

    const gender = optionsGender.filter(genderObj => genderObj.value === user.gender);
    const state = optionsState.filter(stateObj => stateObj.value === user.state);

    this._selectedGender = gender.length > 0 ? gender[0] : {
      label: 'Masculino',
      value: 'M',
    };
    this._selectedState = state.length > 0 ? state[0] : {
      label: '',
      value: '',
    };
    this._selectedCity = {
      label: user.city,
      value: user.city,
    };

    if (user.height.includes('#')) {
      this.props.updateUserProp({
        height: '',
        gender: this._selectedGender.value,
        state: this._selectedState.value,
      });
    } else {
      this.props.updateUserProp({
        gender: this._selectedGender.value,
        state: this._selectedState.value,
      });
    }

    this.state = {
      isLoading: false,
      isCitiesModalVisible: false,
    };
  }

  _nextSignUpScreen = () => {
    const {
            id,
            name,
            email,
            phone,
            gender,
            birthdate,
            height,
            city,
            state,
            marital_status,
            schooling,
            business_sector,
            about,
            preferences,
            goals,
            no_interests,
            radius_around,
            pictures,
            token,
            document,
            document_type,
            user_token,
          } = this.props.user;

    const {isEditProfile} = this.props;

    if (!name || name === '') {
      Alert.alert(
        'Ops... não identificamos seu nome',
        'Essa informação é de preenchimento obrigatório.',
        [
          {
            text: 'OK',
            onPress: () => {},
          },
        ],
        {cancelable: false},
      );
    } else if (!email || email === '') {
      Alert.alert(
        'Ops... não identificamos seu email',
        'Essa informação é de preenchimento obrigatório.',
        [
          {
            text: 'OK',
            onPress: () => {},
          },
        ],
        {cancelable: false},
      );
    } else if (!pictures || pictures.length === 0) {
      Alert.alert(
        'Opa... não identificamos imagens de perfil',
        'Essa informação é de preenchimento obrigatório.',
        [
          {
            text: 'OK',
            onPress: () => {},
          },
        ],
        {cancelable: false},
      );
    } else if (!phone || phone === '') {
      Alert.alert(
        'Opa... não identificamos seu Telefone',
        'Essa informação é de preenchimento obrigatório.',
        [
          {
            text: 'OK',
            onPress: () => {},
          },
        ],
        {cancelable: false},
      );
    } else if (!birthdate || birthdate === '') {
      Alert.alert(
        'Opa... não identificamos sua Data de Nascimento',
        'Essa informação é de preenchimento obrigatório.',
        [
          {
            text: 'OK',
            onPress: () => {},
          },
        ],
        {cancelable: false},
      );
    } else if (!height || height === '') {
      Alert.alert(
        'Opa... não identificamos sua Altura',
        'Essa informação é de preenchimento obrigatório.',
        [
          {
            text: 'OK',
            onPress: () => {},
          },
        ],
        {cancelable: false},
      );
    } else if (!city || city === '') {
      Alert.alert(
        'Opa... não identificamos sua Cidade',
        'Essa informação é de preenchimento obrigatório.',
        [
          {
            text: 'OK',
            onPress: () => {},
          },
        ],
        {cancelable: false},
      );
    } else if (!state || state === '') {
      Alert.alert(
        'Opa... não identificamos seu Estado',
        'Essa informação é de preenchimento obrigatório.',
        [
          {
            text: 'OK',
            onPress: () => {},
          },
        ],
        {cancelable: false},
      );
    } else {
      Navigation.push(this.props.componentId, {
        component: {
          id: 'SecondSignUp',
          name: 'SecondSignUp',
          passProps: {
            isEditProfile,
          },
        },
      });
    }
  };

  _selectPicture = (picture, index) => {
    const {pictures} = this.props.user;
    const {updateUserProp} = this.props;
    const start = pictures.slice(0, index);
    const end = pictures.slice(index + 1);
    const newPicturesArr = start.concat({
      id: null,
      picture,
    }).concat(end);
    //const newPicturesArr = [...pictures, picture];
    updateUserProp({
      pictures: [...newPicturesArr],
    });
  };

  _removePicture = (idx) => {
    const {pictures, id} = this.props.user;
    const {updateUserProp} = this.props;
    const start = pictures.slice(0, idx);
    const end = pictures.slice(idx + 1);
    const newPicturesArr = start.concat(end);

    if (pictures[idx].id) {
      this.props.deletePicture(id, pictures[idx].id, this.props.user)
      .then(({type, data}) => {
        if (DELETE_PICTURE_SUCCESS === type) {
          updateUserProp({
            pictures: newPicturesArr,
          }).then(() => showMessage('Sucesso!', 'Foto removida com sucesso.'));
        } else showMessage('Erro de requisição', 'Ocorreu um erro na exclusão da foto, tente novamente.');
      })
      .catch(_ => showMessage('Erro de requisição', 'Ocorreu um erro na exclusão da foto, tente novamente.'));
    } else {
      updateUserProp({
        pictures: newPicturesArr,
      }).then(() => showMessage('Sucesso!', 'Foto removida com sucesso.'));
    }
  };

  _keyExtractor = (item, index) => index.toString();

  _renderItem = ({item, index}) => {
    return (
      <ProfilePicture
        item={item}
        index={index}
        selectPicture={this._selectPicture}
        removePicture={this._removePicture}
      />
    );
  };

  componentDidMount(): void {
    const user = this.props.user;
    if (user.state && user.state !== '') {
      this.setState({isLoading: true}, () => {
        this.props.fetchCities(user.state)
        .then(({data}) => {
          this.setState({isLoading: false});
          if (user.city && user.city !== '') {
            const city = data.find(item => item.name.includes(user.city));
            this._selectedCity = {
              label: city,
              value: city,
            };
          }
        })
        .catch(_ => this.setState({isLoading: false}));
      });
    }
  }

  render() {
    const {isLoading, isCitiesModalVisible} = this.state;
    const {name, email, phone, birthdate, height, city, pictures} = this.props.user;
    const {updateUserProp, cities} = this.props;

    const newPictures = pictures && pictures.length < 4 ? [...pictures, {
      id: null,
      picture: '',
    }] : pictures;

    return (
      <View style={styles.container}>
        <Header backButton={true} componentId={this.props.componentId}/>
        <ScrollView style={{flex: 1}}>
          <View style={{
            padding: 15,
            paddingTop: 0,
            marginBottom: 15,
            justifyContent: 'space-between',
          }}>
            <Text style={[styles.label, {marginBottom: 15}]}>Fotos de perfil:</Text>
            <FlatList
              data={newPictures}
              keyExtractor={this._keyExtractor}
              renderItem={this._renderItem}
              horizontal={true}
            />
            <Text style={styles.label}>Nome completo:</Text>
            <TextInput style={styles.textInput} underlineColorAndroid='transparent'
                       onChangeText={(name) => updateUserProp({name})} value={name}
            />
            <Text style={styles.label}>E-mail:</Text>
            <TextInput editable={false} style={styles.textInput} underlineColorAndroid='transparent'
                       onChangeText={(email) => updateUserProp({email})} value={email}
                       autoCapitalize={'none'}/>
            <View style={{flexDirection: 'row'}}>
              <View style={{
                flex: 1,
                marginRight: 15,
              }}>
                <Text style={styles.label}>Celular:</Text>
                <TextInputMask
                  type={'cel-phone'}
                  options={{
                    withDDD: true,
                  }}
                  style={styles.textInput}
                  underlineColorAndroid='transparent'
                  onChangeText={(phone) => updateUserProp({phone})}
                  value={phone}
                  maxLength={15}
                />
              </View>
              <View style={{width: 90}}>
                <Text style={styles.label}>Gênero:</Text>
                <Picker
                  value={this._selectedGender}
                  onChange={gender => {
                    this._selectedGender = gender;
                    updateUserProp({gender: gender.value});
                  }}
                  rightIconSource={dropdown}
                  containerStyle={{
                    borderBottomWidth: 1,
                    borderColor: '#B54D80',
                    height: 40,
                  }}>
                  {optionsGender.map(option => <Picker.Item key={option.value} value={option}
                                                            disabled={option.disabled}/>)}
                </Picker>
              </View>
            </View>
            <View style={{flexDirection: 'row'}}>
              <View style={{
                flex: 1,
                marginRight: 15,
              }}>
                <Text style={styles.label}>Data Nascimento:</Text>
                <TextInputMask
                  type={'datetime'}
                  options={{
                    format: 'DD/MM/YYYY',
                  }}
                  style={styles.textInput}
                  underlineColorAndroid='transparent'
                  onChangeText={(birthdate) => updateUserProp({birthdate})}
                  value={birthdate}
                  maxLength={10}
                />
              </View>
              <View style={{width: 90}}>
                <Text style={styles.label}>Altura:</Text>
                <TextInputMask
                  type={'custom'}
                  options={{
                    mask: '9,99',
                  }}
                  style={styles.textInput}
                  underlineColorAndroid='transparent'
                  onChangeText={(height) => updateUserProp({height})}
                  value={height}
                  maxLength={4}
                  keyboardType='number-pad'
                />
              </View>
            </View>
            <View style={{flexDirection: 'row'}}>
              <View style={{width: 90}}>
                <Text style={styles.label}>Estado:</Text>
                <Picker
                  value={this._selectedState}
                  onChange={state => {
                    this._selectedState = state;
                    updateUserProp({state: state.value});
                    this.setState({isLoading: true}, () => {
                      this.props.fetchCities(state.value)
                      .then(({type, data}) => {
                        this.setState({isLoading: false});
                      })
                      .catch((response) => {
                        console.log('ERRO', response);
                        this.setState({isLoading: false});
                      });
                    });
                  }}
                  rightIconSource={dropdown}
                  containerStyle={{
                    borderBottomWidth: 1,
                    borderColor: '#B54D80',
                    height: 40,
                  }}>
                  {optionsState.map(option => <Picker.Item key={option.value} label={option.value} value={option}
                                                           disabled={option.disabled}/>)}
                </Picker>
              </View>
              <View style={{
                flex: 1,
                marginLeft: 15,
              }}>
                <Text style={styles.label}>Cidade:</Text>
                <TouchableOpacity onPress={() => this.setState({isCitiesModalVisible: true})}>
                  <View style={{
                    height: 40,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    borderBottomWidth: 1,
                    borderColor: '#B54D80',
                  }}>
                    <Text>{city ? city : ''}</Text>
                    <Image
                      style={{
                        width: 14,
                        height: 8,
                      }}
                      source={dropdown}
                    />
                  </View>
                </TouchableOpacity>
                {/*<Picker
                 value={this._selectedCity}
                 onChange={city => {
                 this._selectedCity = city;
                 this.setState({city});
                 }}
                 rightIconSource={dropdown}
                 containerStyle={{
                 borderBottomWidth: 1,
                 borderColor: '#7a1043',
                 height: 40,
                 }}>
                 {
                 cities.map((city, idx) => <Picker.Item key={idx} label={city.name} value={city.name} />)
                 }
                 </Picker>*/}
              </View>
            </View>
          </View>
        </ScrollView>
        <Button
          containerStyle={{
            backgroundColor: '#7A1043',
            borderWidth: 0,
            borderRadius: 0,
          }}
          onPress={this._nextSignUpScreen}
          text='Continuar'
        />
        <Modal
          transparent={false}
          visible={isCitiesModalVisible}
          onRequestClose={() => {
          }}
        >
          <View style={{
            flex: 1,
            backgroundColor: 'FFF',
            marginTop: Platform.OS === 'ios' ? 15 : 0,
          }}>
            <TouchableOpacity onPress={() => this.setState({isCitiesModalVisible: false})}>
              <View style={{
                padding: 15,
                paddingBottom: 5,
                paddingRight: 10,
              }}>
                <Icon name="close" size={30} color={'#7a1043'}/>
              </View>
            </TouchableOpacity>
            <FlatList
              data={cities}
              keyExtractor={(item, index) => index.toString()}
              renderItem={
                ({item}) => {
                  return (
                    <TouchableOpacity onPress={() => {
                      updateUserProp({city: item.name});
                      this.setState({
                        isCitiesModalVisible: false,
                      });
                    }}>
                      <View style={{
                        paddingLeft: 25,
                        justifyContent: 'center',
                        height: 50,
                        borderBottomWidth: 1,
                        borderColor: '#7a1043',
                      }}>
                        <Text>{item.name}</Text>
                      </View>
                    </TouchableOpacity>
                  );
                }
              }
            />
          </View>
        </Modal>
        <LoaderScreen
          isVisible={isLoading}
          message='Carregando cidades...'
        />
      </View>
    );
  }
}

const mapStateToProps = ({user, cities}) => {
  return {
    user,
    cities,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCities: (state) => dispatch(fetchCities(state)),
    deletePicture: (id_usuario, id_foto, user) => dispatch(deletePicture(id_usuario, id_foto, user)),
    updateUserProp: (user) => dispatch(updateUserProp(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FirstSignUp);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  label: {
    color: '#7D7474',
    fontSize: 14,
    marginTop: 15,
  },
  textInput: {
    padding: 0,
    paddingLeft: 15,
    color: '#000',
    fontSize: 15,
    borderBottomWidth: 1,
    borderColor: '#B54D80',
    height: 40,
  },
});
