/**
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Image,
} from 'react-native';

import Header from "../component/header.component";
import Avatar from "react-native-ui-lib/typings/components/avatar";
import * as AvatarHelper from "react-native-ui-lib/typings/helpers/AvatarHelper";
import connect from "react-redux/es/connect/connect";

type Props = {};

class MyPlanScreen extends Component<Props> {

  static get options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    };
  }

  render() {
    const {pictures, name, age, business_sector} = this.props.user;
    const hasProfilePicture = pictures && pictures.length > 0 && pictures[0].picture;

    return (
      <View style={{flex: 1, backgroundColor: '#FFF'}}>
        <Header backButton={true} componentId={this.props.componentId}/>

        <View style={{flex: 1, padding: 15, alignItems: 'center', justifyContent: 'space-between'}}>
          <View style={{alignItems: 'center',}}>
            <View style={{
              borderColor: '#107DFB',
              borderWidth: 4,
              borderRadius: 160,
            }}>
              <Avatar
                  imageSource={{uri: hasProfilePicture ? pictures[0].picture : null}}
                  size={160}
                  label={AvatarHelper.getInitials(name ? name : '')}
                  labelColor={'#000'}
              />
            </View>

            <Text style={{fontSize: 23, color: '#000', marginTop: 10}}>{name}</Text>
            <Text style={{fontSize: 16, color: '#444', marginBottom: 10}}>{age} anos, {business_sector}</Text>

            <TouchableOpacity>
              <View style={{
                justifyContent: 'center',
                alignItems: 'center',
                height: 50,
                borderWidth: 1,
                borderRadius: 5,
                borderColor: '#f55',
                paddingRight: 15,
                paddingLeft: 15,
              }}>
                <Text style={{fontSize: 20, color: '#f55', textAlign: 'center'}}>Plano atual: grátis</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View>
            <Image
              style={{width: 157, height: 23, marginBottom: 15,}}
              source={require('../resource/cartoes.png')}/>
            <TouchableOpacity>
              <View style={{backgroundColor: '#FDDD0E', padding: 10, marginBottom: 15, alignItems: 'center', justifyContent: 'center', borderRadius: 5, borderWidth: 2, borderColor: '#B59C00'}}>
                <Text style={{color: '#000', fontSize: 26, fontWeight: 'bold'}}>
                  Quero ser PREMIUM
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({user}) => {
  return {
    user,
  };
};

export default connect(mapStateToProps, null)(MyPlanScreen);
