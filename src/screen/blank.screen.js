/**
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Text, View } from 'react-native';

import PanResponderExample from '../component/pan.responder.example';

type Props = {};

class BlankScreen extends Component<Props> {

  render() {
    return (
      <View style={{padding: 15}}>
        <Text style={{color: '#222', fontSize: 23}}>Em criação...</Text>
      </View>
    );
  }
}

export default BlankScreen;
