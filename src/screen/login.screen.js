/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Image,
  View,
  Text,
  TouchableOpacity,
  AsyncStorage,
  ImageBackground,
} from 'react-native';
import LinkedInModal from 'react-native-linkedin';
import connect from 'react-redux/es/connect/connect';
import firebase from 'react-native-firebase';
import type {
  Notification,
  NotificationOpen,
} from 'react-native-firebase';

import {userLogin} from '../action/user.action';
import {LOGIN_SUCCESS} from '../constant';
import {Navigation} from 'react-native-navigation';

import {facebookLogin} from '../constant/fbsdk';
import LoaderScreen from '../component/loading.screen.component';
import InitTabNavigation from '../constant/init.tab.navigation';

type Props = {};

class LoginScreen extends Component<Props> {

  static get options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    };
  }

  componentDidMount() {
    console.warn('fcmToken_user_token -> ');
    firebase.messaging().hasPermission()
    .then(enabled => {
      console.warn('enabled', enabled);
      if (enabled) {
        firebase.messaging().getToken()
        .then(user_token => {
          console.warn('fcmToken_user_token -> ', user_token);

        });
      }
    })
    .catch(error => {
      console.log('fcmToken-error -> ', error);
    });
  }

  constructor() {
    super();

    this.state = {
      isLoading: false,
    };
  }

  _updateStateAndRedirect = (accessToken, loginType) => {
    firebase.messaging().hasPermission()
    .then(enabled => {
      console.log('enabled', enabled);
      if (enabled) {
        firebase.messaging().getToken()
        .then(user_token => {
          console.log('fcmToken -> ', user_token);
          this.setState({isLoading: true}, () => {
            this.props.userLogin(accessToken, loginType, user_token)
            .then(({type, data}) => {
              this.setState({isLoading: false});
              if (type === LOGIN_SUCCESS) {
                if (data.redirect_uri === 'users/list') {
                  AsyncStorage.setItem('user', JSON.stringify(data))
                  .then(_ => {
                    InitTabNavigation();
                  });
                } else {
                  Navigation.setRoot({
                    root: {
                      stack: {
                        id: 'InitialStackScreens',
                        children: [
                          {
                            component: {
                              id: 'InitialSignUp',
                              name: 'InitialSignUp',
                            },
                          },
                        ],
                      },
                    },
                  });
                }
              }
            })
            .catch(err => {
              this.setState({isLoading: false});
            });
          });

        });
      }
    })
    .catch(error => {
      console.log('fcmToken-error -> ', error);
    });
  };

  render() {
    const {isLoading} = this.state;

    return (
      <ImageBackground
        source={require('../resource/login-bg.png')}
        style={{flex: 1}}>
        <View style={{
          flex: 1,
          backgroundColor: 'rgba(0, 0, 0, 0.6)',
          padding: 45,
          justifyContent: 'space-between',
        }}>
          <View style={{
            width: 120,
            height: 120,
            borderRadius: 10,
            backgroundColor: '#7a1043',
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            <Image
              style={{
                width: 90,
                height: 90,
              }}
              source={require('../resource/logo.png')}
            />
          </View>
          <View style={{
            height: 120,
            justifyContent: 'space-evenly',
          }}>
            <LinkedInModal
              clientID="78dtfni4ewkc0s"
              clientSecret="SSxbma8oT7W71qOz"
              redirectUri="http://localhost"
              renderButton={() => <View style={{
                justifyContent: 'center',
                alignItems: 'center',
                height: 50,
                borderWidth: 1,
                borderRadius: 5,
                borderColor: '#fff',
              }}>
                <Text style={{
                  fontSize: 20,
                  color: '#fff',
                  textAlign: 'center',
                }}>Entrar com LinkedIn</Text>
              </View>}
              onSuccess={({access_token}) => {
                console.log('access_token', access_token);
                this._updateStateAndRedirect(access_token, 'linkedin');
              }}
            />
            <TouchableOpacity
              onPress={() => facebookLogin(this._updateStateAndRedirect)}
            >
              <View style={{
                justifyContent: 'center',
                alignItems: 'center',
                height: 50,
                borderWidth: 1,
                borderRadius: 5,
                borderColor: '#fff',
              }}>
                <Text style={{
                  fontSize: 20,
                  color: '#fff',
                  textAlign: 'center',
                }}>Entrar com Facebook</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <LoaderScreen
          isVisible={isLoading}
          message='Finalizando login...'
        />
      </ImageBackground>
    );
  }
}

const mapStateToProps = ({user}) => {
  return {
    user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userLogin: (access_token, type_login, user_token) => dispatch(userLogin(access_token, type_login, user_token)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
