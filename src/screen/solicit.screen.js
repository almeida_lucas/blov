/**
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Image, Text, TouchableOpacity, View, FlatList, ToastAndroid} from 'react-native';
import { Avatar, AvatarHelper } from 'react-native-ui-lib';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Header from '../component/header.component';
import connect from "react-redux/es/connect/connect";
import LoaderScreen from "../component/loading.screen.component";
import {fetchMatches, confirmMatch} from "../action/matches.action";
import SolicitItem from "../component/solicit.item.component";
import {AccessToken} from "react-native-fbsdk";
import {Navigation} from "react-native-navigation";
import {fetchLocalUser} from "../action/user.action";
import {CONFIRM_MATCHES_SUCCESS} from "../constant";
import {fetchChatList} from "../action/chat.list.action";
import showMessage from '../constant/native.message';
import axios from 'axios';

type Props = {};

class SolicitScreen extends Component<Props> {

  static get options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
      bottomTab: {
        badgeColor: '#DDA0DD',
      },
    };
  }

  constructor(props) {
    super(props);

    this.state = {
        isLoading: false,
        matches: [],
        isRefreshing: false,
    };
  }

  _pushDeviceMessage = (user_token, name) => {
    console.warn('user_token', user_token);
    if (user_token !== null) {
      axios(`https://us-central1-blov-test.cloudfunctions.net/sendMessageToDevice`, {
        method: 'POST',
        data: {
          notification: {
            title: `Novo chat com ${name}`,
            body: `${name} aceitou sua solicitação!`,
          },
          registrationToken: user_token,
        },
      }).then(({data}) => {
        console.warn('SUCESSO_sendMessageToDevice', data);
      }).catch(message => {
        console.log('ERRO_sendMessageToDevice', message);
      });
    }
  };

  _confirmMatch = (id, target_user_match, user_token) => {
      this.props.confirmMatch(id, target_user_match, this.props.user.token)
          .then(({type}) => {
              if (type === CONFIRM_MATCHES_SUCCESS) {
                //TODO buscar user_token
                this._pushDeviceMessage(user_token, this.props.user.name);

                  this.props.fetchChatList(id, this.props.user.token)
                      .then(({type, data}) => {
                          const chatLength = data.length;
                          if (chatLength !== 0) {
                              console.log('this.props.componentId', this.props.componentId);
                              Navigation.mergeOptions('ChatScreen', {
                                  bottomTab: {
                                      badge: chatLength.toString(),
                                  },
                              });
                          }
                      })
                      .catch((err) => this.setState({isLoading: false}));
              }
          })
          .catch(_ => showMessage('Erro', 'Erro ao confirmar solicitação'));
  };

    _removeItem = (idx) => {
        const {matches} = this.state;
        const start = matches.slice(0, idx);
        const end = matches.slice(idx + 1);

        this.setState({
            matches: start.concat(end),
        });
    };

  _keyExtractor = (item, index) => index.toString();

  _renderItem = ({item, index}) => {
    return(
      <SolicitItem {...item} index={index} removeItem={this._removeItem} confirmMatch={this._confirmMatch}/>
    )
  };

  componentWillMount(): void {
      const {fetchLocalUser, fetchMatches} = this.props;
      this.setState({isLoading: true}, () => {
          fetchLocalUser().then(({type, data}) => {
              // console.log('data - solict', data);
              // console.log('token - solict', data.token);
                  fetchMatches(data.token)
                      .then(({type, data}) => {
                          const solicitLength = data.matches.length;
                          this.setState({matches: data.matches});
                          if (solicitLength !== 0) {
                              Navigation.mergeOptions(this.props.componentId, {
                                  bottomTab: {
                                      badge: solicitLength.toString(),
                                  },
                              });
                          }
                      })
                      .catch((err) => this.setState({isLoading: false}));
          }).catch((err) => this.setState({isLoading: false}));
      });
  }

    _onRefresh = () => {
      this.setState({isRefreshing: true}, () => {
          this.props.fetchMatches(this.props.user.token)
              .then(({type, data}) => {
                  console.log('data -> ', data);
                  const solicitLength = data.matches.length;
                  this.setState({matches: data.matches, isRefreshing: false});
                  if (solicitLength !== 0) {
                      Navigation.mergeOptions(this.props.componentId, {
                          bottomTab: {
                              badge: solicitLength.toString(),
                          },
                      });
                  }
              })
              .catch((err) => {
                  console.log('err -> ', err);
                  this.setState({isLoading: false, isRefreshing: false})
              });
        });
    };

    render() {
    const {isLoading, isRefreshing} = this.state;
    const {matches} = this.state;

    return (
      <View style={{flex: 1, alignItems: 'stretch'}}>
        <Header />
        <FlatList
            refreshing={isRefreshing}
            onRefresh={this._onRefresh}
          data={matches}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}/>
      </View>
    );
  }
}

const mapStateToProps = ({user, matchesList}) => {
    const {matches} = matchesList;

    return {
        user,
        matches,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchMatches: (token) => dispatch(fetchMatches(token)),
        fetchLocalUser: () => dispatch(fetchLocalUser()),
        confirmMatch: (id, target_user_match, token) => dispatch(confirmMatch(id, target_user_match, token)),
        fetchChatList: (id, token) => dispatch(fetchChatList(id, token)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SolicitScreen);
