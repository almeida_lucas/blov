/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  TextInput,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Platform,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import connect from 'react-redux/es/connect/connect';

import {newChatMessage} from '../action/chat.messges.action';
import Header from '../component/header.component';
import * as AvatarHelper from 'react-native-ui-lib/typings/helpers/AvatarHelper';
import Avatar from 'react-native-ui-lib/typings/components/avatar';
import {updateChatList} from '../action/chat.list.action';
import {NEW_CHAT_MESSAGE_SUCCESS} from '../constant';
// import firebase from "react-native-firebase";
import showMessage from '../constant/native.message';
import axios from 'axios';

type Props = {};

class ChatItemScreen extends Component<Props> {

  state = {
    message: '',
  };

  componentDidMount(): void {
  }

  /*_pushDeviceMessage = (userId) => {
   firebase.firestore().collection('users')
   .doc(userId.toString())
   .get()
   .then(documentSnapshot => {
   const registrationToken = documentSnapshot.data().userToken;
   console.warn('registrationToken -> ', registrationToken);

   axios(`https://us-central1-blov-test.cloudfunctions.net/sendMessageToDevice`, {
   method: 'POST',
   data: {
   notification: {
   title: 'Blov',
   body: 'Corpo mensagem',
   },
   registrationToken
   },
   }).then(({data}) => {
   console.warn('SUCESSO_sendMessageToDevice', data);
   }).catch(message => {
   console.warn('ERRO_sendMessageToDevice', message);
   });
   })
   .catch(error => {
   console.warn('registrationToken_error -> ', error);


   });
   };*/

  _pushDeviceMessage = (user_token, name, message) => {
    console.warn('user_token', user_token);
    if (user_token !== null) {
      axios(`https://us-central1-blov-test.cloudfunctions.net/sendMessageToDevice`, {
        method: 'POST',
        data: {
          notification: {
            title: `Nova mensagem de ${name}`,
            body: `${message}`,
          },
          registrationToken: user_token,
        },
      }).then(({data}) => {
        console.warn('SUCESSO_sendMessageToDevice', data);
      }).catch(message => {
        console.warn('ERRO_sendMessageToDevice', message);
      });
    }
  };

  _sendMessage = () => {
    const {newChatMessage, token, updateChatList, user_id, chat_id, chatList, user_token} = this.props;

    const chat = chatList.find(chat => chat.chat_id.toString() === chat_id.toString());
    // const chat = chatList.find(chat => chat.user_id.toString() === user_id.toString());

    const {message} = this.state;
    if (message !== '') {
      const textMessage = message;
      newChatMessage(chat_id, token, message)
      .then(({type, data, message}) => {
        console.log(`THEN - ${type}`, data);
        if (type === NEW_CHAT_MESSAGE_SUCCESS) {
          updateChatList(chat_id, data);
          //TODO buscar user_token
            this._pushDeviceMessage(user_token, this.props.user.name, textMessage);
          this.setState({message: ''});
        } else {
          showMessage('Erro', message);
        }
      })
      .catch(({type, message}) => {
        showMessage('Erro', message);
      });
    }
  };

  _keyExtractor = (item, index) => index.toString();

  _renderItem = ({item}) => {
    const isPositionRight = item.position === 'right';
    return (
      <View style={{
        marginBottom: 15,
        marginTop: 5,
        alignItems: isPositionRight ? 'flex-end' : 'flex-start',
      }}>
        <View style={{
          borderWidth: 1,
          borderColor: isPositionRight ? 'transparent' : '#DDD',
          maxWidth: 200,
          padding: 10,
          paddingRight: 20,
          paddingLeft: 20,
          backgroundColor: isPositionRight ? '#DDA0DD' : '#FFF',
          borderRadius: 5,
        }}>
          <Text style={{flexWrap: 'wrap'}}>{item.message}</Text>
        </View>
      </View>
    );
  };

  _closeChat = () => Navigation.dismissAllModals();

  render() {
    const {message} = this.state;
    const {user_id, chat_id, chatList} = this.props;
    const chat = chatList.find(chat => chat.chat_id.toString() === chat_id.toString());
    // const chat = chatList.find(chat => chat.user_id.toString() === user_id.toString());
    const {user, chat_history, picture} = chat;

    return (
      <View style={{
        flex: 1,
        backgroundColor: '#fff',
        marginTop: Platform.OS === 'ios' ? 15 : 0,
      }}>
        <View style={{
          flexDirection: 'row',
          height: 50,
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingLeft: 15,
          paddingRight: 15,
        }}>
          <TouchableOpacity onPress={this._closeChat}>
            <MaterialIcon name="arrow-back" size={30} color={'#B54D80'}/>
          </TouchableOpacity>
          <View style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
            <Text style={{
              fontSize: 18,
              color: '#444',
              marginRight: 15,
            }}>{user}</Text>
            {
              picture !== '' ?
                <Avatar
                  imageSource={{uri: picture}}
                  size={35}
                />
                :
                <Avatar
                  label={AvatarHelper.getInitials(user ? user : '')}
                  backgroundColor={'#dc9dbc'}
                  labelColor={'#222'}
                  size={40}
                />
            }
          </View>
        </View>
        <FlatList
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'flex-end',
            padding: 15,
          }}
          data={chat_history}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
        />
        <View style={{
          height: 50,
          flexDirection: 'row',
          alignItems: 'flex-start',
          paddingLeft: 5,
        }}>
          <TextInput
            style={{
              height: 40,
              padding: 0,
              paddingLeft: 15,
              color: '#000',
              fontSize: 15,
              flex: 1,
              borderBottomWidth: 1,
              borderColor: '#bbb',
            }}
            underlineColorAndroid='transparent'
            onChangeText={(message) => this.setState({message})}
            value={message}
            placeholder='escreva algo...'
          />
          <TouchableOpacity onPress={this._sendMessage}>
            <View style={{
              flexDirection: 'row',
              width: 50,
              height: 50,
              paddingRight: 15,
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}>
              <Icon name="ios-send" size={32} color={'#7a1043'}/>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({user, chatList}) => {
  return {
    user,
    chatList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    newChatMessage: (chat_id, token, message) => dispatch(newChatMessage(chat_id, token, message)),
    updateChatList: (chat_id, messages) => dispatch(updateChatList(chat_id, messages)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatItemScreen);
