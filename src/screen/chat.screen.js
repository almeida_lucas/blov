/**
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {FlatList, Image, Text, ToastAndroid, TouchableOpacity, View} from 'react-native';
import { Avatar, AvatarHelper } from 'react-native-ui-lib';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Header from '../component/header.component';
import connect from "react-redux/es/connect/connect";
import {fetchLocalUser} from "../action/user.action";
import {fetchMatches} from "../action/matches.action";
import {fetchChatList, updateChatList} from "../action/chat.list.action";
import {receivedChatMessage} from "../action/chat.messges.action";
import {Navigation} from "react-native-navigation";
// import firebase from 'react-native-firebase';
// import type {DocumentReference} from 'react-native-firebase';

type Props = {};

class ChatScreen extends Component<Props> {

  static get options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
      bottomTab: {
        badgeColor: '#DDA0DD',
      },
    };
  }

  constructor() {
      super();

      this.state = {
          isRefreshing: false,
      }
  }

  componentDidMount(): void {
      const {fetchLocalUser, fetchChatList} = this.props;
      this.setState({isLoading: true}, () => {
          fetchLocalUser().then(({type, data}) => {
            const {id, token} = data;
/*
            const fsBatch = firebase.firestore().batch();
            fsBatch.update(firebase.firestore().collection('chat_messages').doc('23'), {chats: [{messages: []}]});

            fsBatch.update(firebase.firestore().collection('chat_messages').doc(id.toString()), {chats: [{messages: [{text: 'AAA'}]}]})
            .commit()
            .then(respP => console.log('chat_messages_set', respP))
            .catch(error => console.log('chat_messages_set_error', error));

            const query = firebase.firestore().collection('chat_messages').where('title', '==', id).get()
            .then((querySnapshot) => {
              console.log(`query`, query);
              console.log(`querySnapshot`, querySnapshot.size);
            });*/

            // Create WebSocket connection.
              const ws = new WebSocket('ws://18.221.183.124:8091');

              ws.onopen = () => {
                  // connection opened
                  console.log("Connection established!");
                  ws.send(JSON.stringify({
                      command: "register",
                      id: data.id,
                  }));
              };

              ws.onmessage = (e) => {
                  // a message was received
                  console.log('nova mensagem!', e.data);
                  const {type, message, chat_id} = JSON.parse(e.data);
                  const {chatList, updateChatList} = this.props;
                  const chat = chatList.find(chat => chat_id && chat.chat_id.toString() === chat_id.toString());

                  if (chat) {
                      console.log('message -- ', {message, position: 'left'});
                      console.log('chat_id -- ', chat_id);
                      console.log('chat -- ', chat);

                      updateChatList(chat_id, {message, position: 'left'});
                  }
              };

              ws.onerror = (e) => {
                  // an error occurred
                  console.log('ERROR', e.message);
              };

              ws.onclose = (e) => {
                  // connection closed
                  console.log(e.code, e.reason);
              };

            fetchChatList(data.id, data.token)
            .then(({type, data}) => {
              const chatLength = data.length;
              if (chatLength !== 0) {
                  Navigation.mergeOptions(this.props.componentId, {
                      bottomTab: {
                          badge: chatLength.toString(),
                      },
                  });
              }
            })
            .catch((err) => this.setState({isLoading: false}));

            // this.unsubscribe = firebase.firestore().collection('chat_messages')
            // .doc(id.toString()).onSnapshot(this._onCollectionUpdate);

          }).catch(_ => this.setState({isLoading: false}));
      });
  }

  componentWillUnmount() {
    // this.unsubscribe();
  }

  _onCollectionUpdate = (querySnapshot) => {
    const {chat_list} = querySnapshot.data();

    console.log('_onCollectionUpdate - doc.data()', querySnapshot.data());
    console.log('_onCollectionUpdate', chat_list);
  };

  _showChatScreen = (chat_id, user_token) => Navigation.showModal({
      component: {
          id: 'ChatItemScreen',
          name: 'ChatItemScreen',
          passProps: {
              chat_id,
            user_token,
              user_id: this.props.user.id,
              token: this.props.user.token,
          },
          options: {
              screenBackgroundColor: 'transparent',
              modalPresentationStyle: 'overCurrentContext',
          },
      },
  });

  _keyExtractor = (item, index) => index.toString();

  _renderItem = ({item}) => {
    const {user, chat_id, picture, user_token} = item;
    return(
      <TouchableOpacity onPress={() => this._showChatScreen(chat_id, user_token)}>
        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          padding: 15,
          paddingTop: 10,
          paddingBottom: 10,
          borderBottomWidth: 1,
          borderColor: '#CCC',
        }}>
          <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>
              {
                  picture !== '' ?
                      <Avatar
                          imageSource={{uri: picture}}
                          size={35}
                      />
                      :
                      <Avatar
                          label={AvatarHelper.getInitials(user)}
                          backgroundColor={'#dc9dbc'}
                          labelColor={'#222'}
                          size={30}
                      />
              }
            <View style={{marginLeft: 10}}>
              <Text style={{color: '#333', fontSize: 20}}>{user}</Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{
              backgroundColor: 'transparent',
              width: 25,
              height: 25,
              borderRadius: 25,
              marginRight: 30,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
              {/*<Text style={{color: '#fff', fontWeight: 'bold'}}>3</Text>*/}
            </View>
            <View>
              <Icon name="chevron-right" size={25} color={'#777'}/>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  };

    _onRefresh = () => {
        this.setState({isRefreshing: true}, () => {
            const {id, token} = this.props.user;

            this.props.fetchChatList(id, token)
                .then(({type, data}) => {
                    this.setState({isRefreshing: false});
                    const chatLength = data.length;
                    if (chatLength !== 0) {
                        Navigation.mergeOptions(this.props.componentId, {
                            bottomTab: {
                                badge: chatLength.toString(),
                            },
                        });
                    }
                })
                .catch((err) => {
                    console.log('err -> ', err);
                    this.setState({isRefreshing: false});
                });
        });
    };

  render() {
      const {isRefreshing} = this.state;
    const {chatList, approved_matches} = this.props;
    return (
      <View style={{flex: 1, alignItems: 'stretch'}}>
        <Header />
        <FlatList
            refreshing={isRefreshing}
            onRefresh={this._onRefresh}
          data={chatList}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}/>
      </View>
    );
  }
}

const mapStateToProps = ({user, matchesList, chatList}) => {
  return {
      user,
      chatList
  };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchLocalUser: () => dispatch(fetchLocalUser()),
        fetchMatches: (token) => dispatch(fetchMatches(token)),
        fetchChatList: (id, token) => dispatch(fetchChatList(id, token)),
        receivedChatMessage: (message) => dispatch(receivedChatMessage(message)),
        updateChatList: (chat_id, message) => dispatch(updateChatList(chat_id, message)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen);
