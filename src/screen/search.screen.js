/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Animated,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  Modal,
} from 'react-native';
import {
  Avatar,
  AvatarHelper,
  Carousel,
  Constants,
} from 'react-native-ui-lib';

import PanResponderLeft from '../component/pan.responder.left';
import PanResponderRight from '../component/pan.responder.right';
import PanResponderTop from '../component/pan.responder.top';
import {changeLike} from '../action/like.action';
import {fetchLocalUser} from '../action/user.action';
import {
  fetchUserList,
  matchRegister,
  filterUserList,
} from '../action/user.list.action';
import connect from 'react-redux/es/connect/connect';

import Header from '../component/header.component';
import Filter from '../component/filter.component';
import {Navigation} from 'react-native-navigation';
import ActionButton from '../component/action.button.component';
import LoaderScreen from '../component/loading.screen.component';
import {
  FETCH_CHAT_LIST_ERROR,
  FETCH_CHAT_LIST_SUCCESS,
  FETCH_LOCAL_USER_ERROR,
  URL,
  USER_LIST_SUCCESS,
  USER_MATCH_SUCCESS,
} from '../constant';
import Icon from 'react-native-vector-icons/Entypo';
import MultiSelection from '../component/multi.selection.component';
import {fetchMatches} from '../action/matches.action';
import {fetchSectorList} from '../action/sectors.action';
import showMessage from '../constant/native.message';
import axios from 'axios';
import firebase from 'react-native-firebase';
import type { Notification, NotificationOpen } from 'react-native-firebase';

type Props = {};

const AnmView = Animated.createAnimatedComponent(View);

class SearchScreen extends Component<Props> {

  static get options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    };
  }

  constructor(props) {
    super(props);

    this._filterView = React.createRef();
    this._opacity = 0;
    this._color = 'transparent';
    this._viewZIndex = -1;
    this._isVisible = false;

    this.state = {
      color: 'transparent',
      viewZIndex: -1,
      width: 0,
      height: 0,
      imgLoaded: false,
      imgIndex: 0,
      isLoading: false,
      isVisible: false,
    };
    this._animatedValue = new Animated.Value(0);
  }

  _startAnimation = (match) => {
    this._startedAnimation = true;
    Animated.timing(this._animatedValue, {
      toValue: 1,
      duration: 800,
    }).start(() => {
      Animated.timing(this._animatedValue, {
        toValue: 0,
        duration: 0,
      }).start(() => {
        this.setState({viewZIndex: -1});
        this._actionSelect(match);
      });
    });
  };

  componentDidMount() {
    //TODO metodos para tratar recebimento de mensagem
    this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
      // Process your notification as required
      // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
      console.log('notificationDisplayedListener', notification);
    });
    this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
      // Process your notification as required
      console.log('notificationListener', notification);
    });
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
      // Get the action triggered by the notification being opened
      const action = notificationOpen.action;
      // Get information about the notification that was opened
      const notification: Notification = notificationOpen.notification;
      console.log('notificationOpenedListener', notificationOpen);
    });

    const {fetchUserList, fetchLocalUser, fetchMatches, fetchSectorList} = this.props;
    this._isSearching = true;
    this.setState({isLoading: true}, () => {
      fetchLocalUser().then(({type, data, message}) => {
        if (type === FETCH_LOCAL_USER_ERROR) {
          this.setState({isLoading: false}, () => {
            showMessage('Erro', message);
          });
          return;
        }
        console.log('data - match', data);
        console.log('token - match', data.token);
        fetchUserList(data.token)
        .then(() => {
          this.setState({isLoading: false});
          this._isSearching = false;
        })
        .catch((err) => {
          this.setState({isLoading: false});
          this._isSearching = false;
        });
        fetchMatches(data.token)
        .then(({type, data}) => {
        })
        .catch((err) => this.setState({isLoading: false}));

        fetchSectorList()
        .then(_ => {
        })
        .catch(_ => {
        });

      }).catch((err) => this.setState({isLoading: false}));
    });
    Navigation.events().registerBottomTabSelectedListener((params: Any) => {
      console.log('params', params);
      const {selectedTabIndex} = params;

      if (selectedTabIndex === 1) {
        Navigation.mergeOptions('SolicitScreen', {
          bottomTab: {
            badge: '',
          },
        });
      } else if (selectedTabIndex === 2) {
        Navigation.mergeOptions('ChatScreen', {
          bottomTab: {
            badge: '',
          },
        });
      }
    });
  }

  componentWillUnmount() {
    this.notificationDisplayedListener();
    this.notificationListener();
    this.notificationOpenedListener();
  }

  _fetchUserList = (data) => {
    this.setState({isLoading: true}, () => {
      fetchUserList(this.props.user.token)
      .then(() => this.setState({isLoading: false}))
      .catch((err) => this.setState({isLoading: false}));
    });
  };

  _keyExtractor = (item, index) => index.toString();

  _renderItem = ({item}) => {
    return (
      <Image
        style={{
          marginTop: 50,
          width: Constants.screenWidth,
          height: Constants.screenHeight,
        }}
        source={{uri: item.picture}}
      />
    );
  };

  componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {

    if (this.props.userList.length === 0 && !this._isSearching) {
      this._isSearching = true;
      this.props.fetchUserList(this.props.user.token)
      .then(() => {
        this.setState({isLoading: false});
        this._isSearching = false;
      })
      .catch((err) => {
        this.setState({isLoading: false});
        this._isSearching = false;
      });
    }
  }

  _pushDeviceMessage = (user_token, match_type, name) => {
    if (user_token !== null) {
      axios(`http://us-central1-blov-test.cloudfunctions.net/sendMessageToDevice`, {
        method: 'POST',
        data: {
          notification: {
            title: `Nova Solicitação de ${name}`,
            body: `${name} enviou uma solicitação de ${match_type} para você.`,
          },
          registrationToken: user_token,
        },
      }).then(({data}) => {
        console.log('SUCESSO_sendMessageToDevice_novo', data);
      }).catch(message => {
        console.log('ERRO_sendMessageToDevice_novo', message);
      });
    }
  };

  _actionSelect = (match, user_token) => {
    const {user, matchRegister, userList} = this.props;
    const match_type = match.match.toString() === '0' ? 'Dislike' : (match.match.toString() === '1' ? 'Like' : 'Parceria');
    // this.setState({viewZIndex: -1, color: 'transparent', isLoading: true});
    this.setState({isLoading: true}, () => {
      matchRegister(user.token, [match])
      .then(({type, data}) => {
        this.setState({isLoading: false}, () => {
          if (type !== USER_MATCH_SUCCESS)
            showMessage('Erro', 'Erro ao finalizar ação :(');
          else if (match_type !== 'Dislike')
            this._pushDeviceMessage(user_token, match_type, user.name);
        });
      })
      .catch(({type, message}) => {
        this.setState({isLoading: false}, () => {
          showMessage('Erro', message);
        });
      });
    });
  };

  _filterUserList = (token, data) => {
    this._isVisible = !this._isVisible;
    this.setState({isLoading: true}, () => {
      this.props.filterUserList(token, data)
      .then(({type, data, message}) => {
        this.setState({isLoading: false}, () => {
          if (type !== USER_LIST_SUCCESS)
            showMessage('Erro', message);
        });
      })
      .catch(({message}) => {
        this.setState({isLoading: false}, () => {
          showMessage('Erro', message);
        });
      });
    });
  };

  render() {
    const {user, userList, sectorList} = this.props;
    const {width, height, imgLoaded, imgIndex, isLoading, isVisible} = this.state;
    const {pictures} = user;

    let matchUser = {
      name: '',
      email: '',
      age: '',
    };

    if (userList.length > 0) {
      matchUser = userList[0];
      console.log('MATCH_USER', matchUser);
    }

    /*if (!imgLoaded && pictures && pictures.length > 0)
     Image.getSize(pictures[1].picture, (width,height) => {
     console.log('width', width);
     console.log('height', height);

     this.setState({width, height, imgLoaded: true});
     }, (error) => console.log('erro ao carregar imagem', error));*/

    return (
      <View
        style={{
          flex: 1,
          alignItems: 'stretch',
          backgroundColor: '#fff',
        }}>
        <Header filterButton={{
          onPress: () => {
            if (!this._isVisible) {
              this._filterView._startAnimation();
            } else {
              this._filterView._endAnimation();
            }
            this._isVisible = !this._isVisible;
          },
        }}/>
        {
          user.id !== 0 ?
            <Filter
              ref={filterView => {
                if (filterView != null)
                  this._filterView = filterView;
              }}
              {...user}
              search={this._filterUserList}
              sectorList={sectorList}
            />
            :
            null
        }

        {userList.length > 0 ?
          <View
            style={{
              flex: 1,
              alignItems: 'stretch',
              justifyContent: 'flex-start',
            }}>
            <View style={{
              padding: 15,
              alignItems: 'stretch',
              position: 'absolute',
              left: 0,
              top: 0,
              right: 0,
              zIndex: 23,
            }}>
              <View style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginBottom: 10,
              }}>
                {
                  userList[0].pictures && userList[0].pictures.length > 0 ?
                    userList[0].pictures.map((picture, index) => {
                      return (
                        <View
                          key={index}
                          style={{
                            width: 60,
                            height: 10,
                            backgroundColor: '#FFF',
                            opacity: imgIndex === index ? 1 : 0.6,
                            borderRadius: 20,
                            marginRight: 10,
                          }}
                        />
                      );
                    })
                    :
                    null
                }
              </View>
              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
                <View style={{flex: 1}}>
                  <Text style={{
                    fontSize: 35,
                    color: '#FFF',
                    textShadowColor: '#333',
                    textShadowRadius: 5,
                    textShadowOffset: {
                      width: 1,
                      height: 2,
                    },
                  }}>{matchUser.name},</Text>
                  <Text style={{
                    fontSize: 20,
                    color: '#FFF',
                    textShadowColor: '#333',
                    textShadowRadius: 5,
                    textShadowOffset: {
                      width: 1,
                      height: 2,
                    },
                  }}>{matchUser.age} anos - {matchUser.business_setor}</Text>
                </View>
                <TouchableOpacity onPress={() => this.setState({isVisible: true})}>
                  <View style={{marginTop: 15}}>
                    <Image
                      source={require('../resource/info-image.png')}
                      style={{
                        width: 49,
                        height: 52,
                      }}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            {userList[0].pictures && userList[0].pictures.length > 0 ?
              <Carousel
                onChangePage={(imgIndex => this.setState({imgIndex}))}>
                {
                  userList[0].pictures.map((item, index) => {
                    return (
                      <Image
                        key={index}
                        style={{
                          width: Constants.screenWidth,
                          height: Constants.screenHeight,
                        }}
                        source={{uri: item.picture}}
                      />
                    );
                  })
                }
              </Carousel>
              :
              null
            }

            <View style={{
              position: 'absolute',
              bottom: 20,
              alignSelf: 'center',
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              backgroundColor: '#FFF',
              borderRadius: 30,
            }}>
              <TouchableOpacity
                //TODO buscar user_token
                onPress={() => this._actionSelect({
                  id: matchUser.id,
                  match: '0',
                }, matchUser.user_token)}
                style={{
                  padding: 15,
                  paddingLeft: 20,
                  paddingRight: 20,
                }}>
                <Image
                  source={require('../resource/dislike-image.png')}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this._actionSelect({
                  id: matchUser.id,
                  match: '2',
                }, matchUser.user_token)}
                style={{
                  padding: 15,
                  paddingLeft: 20,
                  paddingRight: 20,
                }}>
                <Image
                  source={require('../resource/business-icon.png')}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this._actionSelect({
                  id: matchUser.id,
                  match: '1',
                }, matchUser.user_token)}
                style={{
                  padding: 15,
                  paddingLeft: 20,
                  paddingRight: 20,
                }}>
                <Image
                  source={require('../resource/like-image.png')}
                />
              </TouchableOpacity>
            </View>

            {/*<ActionButton
             ref={component => this._dislikeComponent = component}
             onActionSelect={() => {
             this.setState({viewZIndex: 3, color: '#777'}, () => {
             this._startAnimation({id: matchUser.id, match: '0'});
             });
             }}
             containerStyle={{
             position: 'absolute',
             bottom: 15,
             left: 45,
             alignItems: 'center',
             justifyContent: 'center',
             backgroundColor: '#777',
             }}
             image={require('../resource/ic_thumb_down_24px.png')}
             />

             <ActionButton
             ref={component => this._partnerComponent = component}
             onActionSelect={() => {
             this.setState({viewZIndex: 3, color: '#107cfb'}, () => {
             this._startAnimation({id: matchUser.id, match: '2'});
             });
             }}
             containerStyle={{
             position: 'absolute',
             bottom: 15,
             alignItems: 'center',
             justifyContent: 'center',
             alignSelf: 'center',
             backgroundColor: '#107cfb',
             }}
             image={require('../resource/coffee_icon.png')}
             />

             <ActionButton
             ref={component => this._likeComponent = component}
             onActionSelect={() => {
             this.setState({viewZIndex: 3, color: '#ff4e4e'}, () => {
             this._startAnimation({id: matchUser.id, match: '1'});
             });
             }}
             containerStyle={{
             position: 'absolute',
             bottom: 15,
             right: 45,
             alignItems: 'center',
             justifyContent: 'center',
             backgroundColor: '#ff4e4e',
             }}
             image={require('../resource/For-You-Icon.png')}
             />
             <AnmView
             style={{
             position: 'absolute',
             backgroundColor: this.state.color,
             zIndex: this.state.viewZIndex,
             left: 0,
             top: 0,
             bottom: 0,
             right: 0,
             opacity: this._animatedValue.interpolate({
             inputRange: [0, 1],
             outputRange: [0, 0.8],
             }),
             }}
             />*/}
          </View>
          :
          <View style={{
            padding: 15,
            flex: 1,
            zIndex: 1,
          }}>
            <Text>Carregando lista de usuários...</Text>
          </View>
        }

        <LoaderScreen
          isVisible={isLoading}
          message='Carregando...'
        />


        <Modal
          transparent={true}
          visible={isVisible}
          onRequestClose={() => {
          }}
        >
          <View style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'transparent',
            padding: 15,
          }}>
            <View style={{
              backgroundColor: 'rgba(0, 0, 0, 0.9)',
              padding: 15,
              borderRadius: 15,
              paddingTop: 0,
            }}>
              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
                <View style={{
                  marginRight: 15,
                  marginTop: 15,
                }}>
                  <Text style={{
                    fontSize: 20,
                    color: '#FFF',
                  }}>{matchUser.name}</Text>
                  <Text style={{
                    fontSize: 15,
                    color: '#FFF',
                  }}>{matchUser.age} anos - {matchUser.business_setor}</Text>
                </View>
                <TouchableOpacity onPress={() => this.setState({isVisible: false})}>
                  <View style={{marginTop: 10}}>
                    <Icon name='circle-with-cross' size={30} color={'#fff'}/>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{
                backgroundColor: '#FFF',
                marginTop: 10,
                padding: 15,
                paddingTop: 0,
              }}>
                <View style={{
                  flexDirection: 'row',
                  marginTop: 10,
                }}>
                  <Text style={{
                    color: '#999',
                    fontSize: 14,
                  }}>Estado Civil:</Text>
                  <Text style={{
                    color: '#000',
                    fontSize: 15,
                    marginLeft: 10,
                  }}>{matchUser.marital_status}</Text>
                </View>
                <View style={{
                  flexDirection: 'row',
                  marginTop: 5,
                }}>
                  <Text style={{
                    color: '#999',
                    fontSize: 14,
                  }}>Escolaridade:</Text>
                  <Text style={{
                    color: '#000',
                    fontSize: 15,
                    marginLeft: 10,
                  }}>{matchUser.schooling}</Text>
                </View>
                <View style={{
                  flexDirection: 'row',
                  marginTop: 5,
                }}>
                  <Text style={{
                    color: '#999',
                    fontSize: 14,
                  }}>Setor que empreende:</Text>
                  <Text style={{
                    color: '#000',
                    fontSize: 15,
                    marginLeft: 10,
                  }}>{matchUser.business_setor}</Text>
                </View>
                <Text style={{
                  color: '#999',
                  fontSize: 14,
                  marginTop: 20,
                }}>Tem preferência em:</Text>
                <MultiSelection
                  values={matchUser.preferences}
                  onChange={(preferences) => {}}
                  items={[
                    {
                      label: 'Mulheres',
                      value: 'mulheres',
                    },
                    {
                      label: 'Homens',
                      value: 'homens',
                    },
                    {
                      label: 'Outros',
                      value: 'outros',
                    },
                  ]}
                  isDisabled={true}
                />
                <Text style={{
                  color: '#999',
                  fontSize: 14,
                  marginTop: 20,
                }}>Seus objetivos são:</Text>
                <MultiSelection
                  values={matchUser.goals}
                  onChange={(goals) => {}}
                  items={[
                    {
                      label: 'Negócios',
                      value: 'negocios',
                    },
                    {
                      label: 'Relacionamentos',
                      value: 'relacionamentos',
                    },
                    {
                      label: 'Networking',
                      value: 'networking',
                    },
                    {
                      label: 'Amizades',
                      value: 'amizades',
                    },
                    {
                      label: 'Outros',
                      value: 'outros',
                    },
                  ]}
                  isDisabled={true}
                />
                <Text style={{
                  color: '#999',
                  fontSize: 14,
                  marginTop: 20,
                }}>Não tem interesse em:</Text>
                <MultiSelection
                  values={matchUser.no_interests}
                  onChange={(no_interests) => {}}
                  items={[
                    {
                      label: 'Fumantes',
                      value: 'fumantes',
                    },
                    {
                      label: 'Bebidas Alcoólicas',
                      value: 'bebidas_alcoolicas',
                    },
                    {
                      label: 'Night',
                      value: 'night',
                    },
                    {
                      label: 'Fitness',
                      value: 'fitness',
                    },
                    {
                      label: 'Encontros casuais',
                      value: 'encontros_casuais',
                    },
                  ]}
                  isDisabled={true}
                />
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = ({user, userList, sectorList}) => {
  return {
    user,
    userList,
    sectorList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeLike: () => dispatch(changeLike()),
    fetchLocalUser: () => dispatch(fetchLocalUser()),
    fetchUserList: (token) => dispatch(fetchUserList(token)),
    matchRegister: (token, match) => dispatch(matchRegister(token, match)),
    filterUserList: (token, data) => dispatch(filterUserList(token, data)),
    fetchMatches: (token) => dispatch(fetchMatches(token)),
    fetchSectorList: () => dispatch(fetchSectorList()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);
