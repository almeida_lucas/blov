/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  ScrollView,
  Slider,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  FlatList,
  Modal,
  Image,
  PermissionsAndroid,
  Platform,
  Alert,
} from 'react-native';
import {Picker} from 'react-native-ui-lib';
import {Navigation} from 'react-native-navigation';
import dropdown from '../resource/chevronDown.png';
import Button from '../component/button.component';
import MultiSelection from '../component/multi.selection.component';
import InitTabNavigation from '../constant/init.tab.navigation';
import connect from 'react-redux/es/connect/connect';
import {
  updateUserProp,
  userRegister,
} from '../action/user.action';
import {
  FETCH_CITY_COORDS_SUCCESS,
  REGISTER_SUCCESS,
} from '../constant';
import LoaderScreen from '../component/loading.screen.component';
import Header from '../component/header.component';
import {fetchSectorList} from '../action/sectors.action';
import {fetchCoordsByCity} from '../action/coordinates.action';
import Icon from 'react-native-vector-icons/EvilIcons';
import showMessage from '../constant/native.message';

const optionsMaritalStatus = [
  {
    label: 'Solteiro',
    value: 'solteiro',
  },
  {
    label: 'Casado',
    value: 'casado',
  },
  {
    label: 'Divorciado',
    value: 'divorciado',
  },
  {
    label: 'Viuvo',
    value: 'viuvo',
  },
];
const optionsSchooling = [
  {
    label: 'Superior completo',
    value: 'superior_completo',
  },
  {
    label: 'Superior incompleto',
    value: 'superior_incompleto',
  },
];
const optionsDocumentType = [
  {
    label: '',
    value: '',
  },
  {
    label: 'CNPJ',
    value: 'cnpj',
  },
  {
    label: 'CPF',
    value: 'cpf',
  },
  {
    label: 'OAB',
    value: 'oab',
  },
  {
    label: 'CRC',
    value: 'crc',
  },
  {
    label: 'CRO',
    value: 'cro',
  },
  {
    label: 'CRM',
    value: 'crm',
  },
  {
    label: 'CREA',
    value: 'crea',
  },
];

const optionsBusinessSector = [
  {
    label: '',
    value: '',
  },
  {
    label: 'Tecnologia',
    value: 'tecnologia',
  },
  {
    label: 'Finanças',
    value: 'financas',
  },
];

type Props = {};

class SecondSignUp extends Component<Props> {

  static get options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    };
  }

  constructor(props) {
    super(props);
    const {user, isEditProfile} = props;
    console.log('user--', user);

    const marital_status = optionsMaritalStatus.find(maritalStatusObj => maritalStatusObj.value === user.marital_status);
    const schooling = optionsSchooling.find(optionsSchoolingObj => optionsSchoolingObj.value === user.schooling);
    const document_type = optionsDocumentType.find(optionsDocumentTypeObj => optionsDocumentTypeObj.value === user.document_type);
    // const business_sector = optionsBusinessSector.find(optionsBusinessSectorObj => optionsBusinessSectorObj.value === user.business_sector);
    console.log('document_type--', document_type);

    this._selectedMaritalStatus = marital_status ? marital_status : {
      label: 'Solteiro',
      value: 'solteiro',
    };
    this._selectedSchooling = schooling ? schooling : {
      label: 'Superior completo',
      value: 'superior_completo',
    };
    this._document_type = document_type ? document_type : {
      label: '',
      value: '',
    };

    this.props.updateUserProp({
      marital_status: this._selectedMaritalStatus.value,
      schooling: this._selectedSchooling.value,
      document_type: this._document_type.value,
    });

    this.state = {
      business_sector_text: '',
      sliderShowValue: user.radius_around ? parseInt(user.radius_around) : 1,
      isBusinessSectorVisible: false,
      isLoading: false,
      loaderMessage: '',
      isEditProfile,
    };
  }

  componentDidMount(): void {
    const {updateUserProp, user} = this.props;

    this.setState({
      isLoading: true,
      loaderMessage: 'Carregando setor de empreendimento...',
    }, () => {
      this.props.fetchSectorList()
      .then(_ => {
        const sector = this.props.sectorList.find(item => item.name === user.business_sector);
        this.setState({
          isLoading: false,
          business_sector_text: sector ? sector.name : '',
        }, () => {
          updateUserProp({
            business_sector: sector ? sector.id : '',
          });
        });
      })
      .catch(_ => this.setState({
        isLoading: false,
        business_sector_text: '',
      }, () => {
        updateUserProp({
          business_sector: '',
        });
      }));
    });
  }

  _startInitialTabs = (user, token) => {
    this.setState({
      isLoading: true,
      loaderMessage: 'Finalizando aguarde...',
    }, () => {
      this.props.userRegister(user, token)
      .then(({data, type, message}) => {
        this.setState({isLoading: false}, () => {
          if (type === REGISTER_SUCCESS) {
            if (this.props.isEditProfile)
              Navigation.popToRoot(this.props.componentId);
            else
              InitTabNavigation();
          } else {
            console.log('err - message', message);
            showMessage('Erro', `Erro ao gravar usuario - ${message}`);
          }
        });
      })
      .catch(err => {
        this.setState({isLoading: false}, () => {
          console.log('err', err);
          showMessage('Erro', 'Erro ao gravar usuario');
        });
      });
    });
  };

  _userRegister = async () => {
    const {
            id,
            name,
            email,
            phone,
            gender,
            birthdate,
            height,
            city,
            state,
            marital_status,
            schooling,
            business_sector,
            about,
            preferences,
            goals,
            no_interests,
            radius_around,
            pictures,
            token,
            document,
            document_type,
            user_token,
          } = this.props.user;

    let latitude = -23.810303;
    let longitude = -47.4665071;

    let is_document = true;

    if (!document || document === '') {
      is_document = false;
    }

    const user = {
      id,
      phone,
      gender,
      birthdate: birthdate ? `${birthdate.substr(6)}-${birthdate.substr(3, 2)}-${birthdate.substr(0, 2)}` : '',
      height,
      city,
      state,
      marital_status,
      schooling,
      business_sector,
      about,
      preferences,
      goals,
      no_interest: no_interests,
      radius_around,
      pictures: pictures.map(item => {return item.picture;}),
      latitude,
      longitude,
      document: document ? document : '',
      document_type,
      user_token,
    };

    if (!is_document)
      Alert.alert(
        'Ops...não identificamos sua atividade profissional.',
        'Em breve esta informação será obrigatória para utilização do APP.',
        [
          {
            text: 'OK',
            onPress: async () => {
              //TODO **Código duplicado**
              if (Platform.OS === 'ios') {
                navigator.geolocation.getCurrentPosition((position) => {
                    latitude = position.coords.latitude;
                    longitude = position.coords.longitude;
                    console.log('Localização encontrada com sucesso.', {
                      ...user,
                      latitude,
                      longitude,
                    });
                    this._startInitialTabs({
                      ...user,
                      latitude,
                      longitude,
                    }, token);
                  },
                  (error) => {
                    showMessage(
                      'Error ao buscar localização.',
                      'Usando localização da sua cidade - ' + city + '. Para utilizar sua localização real ative o serviço de localização nas configurações do seu dispositivo.',
                      () => this.props.fetchCoordsByCity(city, state)
                      .then(({type, data}) => {
                        if (type === FETCH_CITY_COORDS_SUCCESS) {
                          console.log(`FETCH_CITY_COORDS_SUCCESS - signup`, data);
                          const {results} = data;
                          const {geometry} = results[0];
                          const {location} = geometry;
                          const {lat, lng} = location;
                          latitude = lat;
                          longitude = lng;
                          this._startInitialTabs({
                            ...user,
                            latitude,
                            longitude,
                          }, token);
                        }
                      })
                      .catch(_ => showMessage('Erro', 'Erro ao buscar localização por cidade')),
                    );
                  },
                  {
                    enableHighAccuracy: false,
                    timeout: 30000,
                    maximumAge: 0,
                  },
                );

                // fetchLocationAndRedirect(user, this._startInitialTabs, token, navigator);
              } else {
                try {
                  const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    {
                      'title': 'B.lov permissão de localização',
                      'message': 'Deseja permitir que o B.lov acesse a sua localização atual para finalizar o cadastro?',
                    },
                  );

                  console.log('granted', granted);
                  if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log('You can use the location');
                    navigator.geolocation.getCurrentPosition((position) => {
                        console.log('position', position);
                        let latitude = position.coords.latitude;
                        let longitude = position.coords.longitude;
                        console.log('Localização encontrada com sucesso.', {
                          ...user,
                          latitude,
                          longitude,
                        });
                        this._startInitialTabs({
                          ...user,
                          latitude,
                          longitude,
                        }, token);
                      },
                      (error) => {
                        console.log('Error ao buscar localização.', error);

                        showMessage('Erro', 'Error ao buscar localização. Usando localização da sua cidade - ' + city);

                        this.props.fetchCoordsByCity(city, state)
                        .then(({type, data}) => {
                          if (type === FETCH_CITY_COORDS_SUCCESS) {
                            console.log(`FETCH_CITY_COORDS_SUCCESS - signup`, data);
                            const {results} = data;
                            const {geometry} = results[0];
                            const {location} = geometry;
                            const {lat, lng} = location;
                            latitude = lat;
                            longitude = lng;
                            this._startInitialTabs({
                              ...user,
                              latitude,
                              longitude,
                            }, token);
                          }
                        })
                        .catch(_ => showMessage('Erro', 'Erro ao buscar localização por cidade'));

                      },
                      {
                        enableHighAccuracy: false,
                        timeout: 10000,
                        maximumAge: 0,
                      },
                    );
                  } else {
                    showMessage('Erro', 'Error ao buscar localização. Usando localização da sua cidade - ' + city);

                    this.props.fetchCoordsByCity(city, state)
                    .then(({type, data}) => {
                      if (type === FETCH_CITY_COORDS_SUCCESS) {
                        console.log(`FETCH_CITY_COORDS_SUCCESS - signup`, data);
                        const {results} = data;
                        const {geometry} = results[0];
                        const {location} = geometry;
                        const {lat, lng} = location;
                        latitude = lat;
                        longitude = lng;
                        this._startInitialTabs({
                          ...user,
                          latitude,
                          longitude,
                        }, token);
                      }
                    })
                    .catch(_ => showMessage('Erro', 'Erro ao buscar localização por cidade'));
                  }
                } catch (err) {
                  console.log(err);
                }
              }
              //TODO **Código duplicado**
            },
          },
        ],
        {cancelable: false},
      );
    else {
      //TODO **Código duplicado**
      if (Platform.OS === 'ios') {
        navigator.geolocation.getCurrentPosition((position) => {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
            console.log('Localização encontrada com sucesso.', {
              ...user,
              latitude,
              longitude,
            });
            this._startInitialTabs({
              ...user,
              latitude,
              longitude,
            }, token);
          },
          (error) => {
            showMessage(
              'Error ao buscar localização.',
              'Usando localização da sua cidade - ' + city + '. Para utilizar sua localização real ative o serviço de localização nas configurações do seu dispositivo.',
              () => this.props.fetchCoordsByCity(city, state)
              .then(({type, data}) => {
                if (type === FETCH_CITY_COORDS_SUCCESS) {
                  console.log(`FETCH_CITY_COORDS_SUCCESS - signup`, data);
                  const {results} = data;
                  const {geometry} = results[0];
                  const {location} = geometry;
                  const {lat, lng} = location;
                  latitude = lat;
                  longitude = lng;
                  this._startInitialTabs({
                    ...user,
                    latitude,
                    longitude,
                  }, token);
                }
              })
              .catch(_ => showMessage('Erro', 'Erro ao buscar localização por cidade')),
            );
          },
          {
            enableHighAccuracy: false,
            timeout: 30000,
            maximumAge: 0,
          },
        );

        // fetchLocationAndRedirect(user, this._startInitialTabs, token, navigator);
      } else {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              'title': 'B.lov permissão de localização',
              'message': 'Deseja permitir que o B.lov acesse a sua localização atual para finalizar o cadastro?',
            },
          );

          console.log('granted', granted);
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('You can use the location');
            navigator.geolocation.getCurrentPosition((position) => {
                console.log('position', position);
                let latitude = position.coords.latitude;
                let longitude = position.coords.longitude;
                console.log('Localização encontrada com sucesso.', {
                  ...user,
                  latitude,
                  longitude,
                });
                this._startInitialTabs({
                  ...user,
                  latitude,
                  longitude,
                }, token);
              },
              (error) => {
                console.log('Error ao buscar localização.', error);

                showMessage('Erro', 'Error ao buscar localização. Usando localização da sua cidade - ' + city);

                this.props.fetchCoordsByCity(city, state)
                .then(({type, data}) => {
                  if (type === FETCH_CITY_COORDS_SUCCESS) {
                    console.log(`FETCH_CITY_COORDS_SUCCESS - signup`, data);
                    const {results} = data;
                    const {geometry} = results[0];
                    const {location} = geometry;
                    const {lat, lng} = location;
                    latitude = lat;
                    longitude = lng;
                    this._startInitialTabs({
                      ...user,
                      latitude,
                      longitude,
                    }, token);
                  }
                })
                .catch(_ => showMessage('Erro', 'Erro ao buscar localização por cidade'));

              },
              {
                enableHighAccuracy: false,
                timeout: 10000,
                maximumAge: 0,
              },
            );
          } else {
            showMessage('Erro', 'Error ao buscar localização. Usando localização da sua cidade - ' + city);

            this.props.fetchCoordsByCity(city, state)
            .then(({type, data}) => {
              if (type === FETCH_CITY_COORDS_SUCCESS) {
                console.log(`FETCH_CITY_COORDS_SUCCESS - signup`, data);
                const {results} = data;
                const {geometry} = results[0];
                const {location} = geometry;
                const {lat, lng} = location;
                latitude = lat;
                longitude = lng;
                this._startInitialTabs({
                  ...user,
                  latitude,
                  longitude,
                }, token);
              }
            })
            .catch(_ => showMessage('Erro', 'Erro ao buscar localização por cidade'));
          }
        } catch (err) {
          console.log(err);
        }
      }
      //TODO **Código duplicado**

    }
  };

  render() {
    const {isLoading, loaderMessage, sliderShowValue, isBusinessSectorVisible, business_sector_text} = this.state;
    const {document_type, radius_around, business_sector, about, preferences, goals, no_interests, document} = this.props.user;
    const {updateUserProp, sectorList} = this.props;

    return (
      <View style={styles.container}>
        <Header backButton={true} componentId={this.props.componentId}/>
        <ScrollView style={{flex: 1}}>
          <View style={{
            padding: 15,
            justifyContent: 'space-between',
          }}>
            <Text style={{
              color: '#333333',
              fontSize: 23,
            }}>Fale um pouco mais sobre você...</Text>
            <Text style={styles.label}>Estado civil:</Text>
            <Picker
              value={this._selectedMaritalStatus}
              onChange={marital_status => {
                this._selectedMaritalStatus = marital_status;
                updateUserProp({marital_status: marital_status.value});
              }}
              rightIconSource={dropdown}
              containerStyle={{
                borderBottomWidth: 1,
                borderColor: '#B54D80',
                height: 40,
              }}>
              {optionsMaritalStatus.map(option => <Picker.Item key={option.value} value={option}
                                                               disabled={option.disabled}/>)}
            </Picker>
            <Text style={styles.label}>Escolaridade:</Text>
            <Picker
              value={this._selectedSchooling}
              onChange={schooling => {
                this._selectedSchooling = schooling;
                updateUserProp({schooling: schooling.value});
              }}
              rightIconSource={dropdown}
              containerStyle={{
                borderBottomWidth: 1,
                borderColor: '#B54D80',
                height: 40,
              }}>
              {optionsSchooling.map(option => <Picker.Item key={option.value} value={option}/>)}
            </Picker>
            <Text style={styles.label}>Setor que empreende:</Text>
            <TouchableOpacity onPress={() => this.setState({isBusinessSectorVisible: true})}>
              <View style={{
                height: 40,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                borderBottomWidth: 1,
                borderColor: '#B54D80',
              }}>
                <Text>{business_sector_text ? business_sector_text : ''}</Text>
                <Image
                  style={{
                    width: 14,
                    height: 8,
                  }}
                  source={dropdown}
                />
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Tipo do documento:</Text>
            <Picker
              value={this._document_type}
              onChange={document_type => {
                this._document_type = document_type;
                updateUserProp({document_type: document_type.value});
              }}
              rightIconSource={dropdown}
              containerStyle={{
                borderBottomWidth: 1,
                borderColor: '#B54D80',
                height: 40,
              }}>
              {optionsDocumentType.map(option => <Picker.Item key={option.value} value={option}/>)}
            </Picker>
            <Text style={styles.label}>Número do documento:</Text>
            <TextInput
              style={[styles.textInput, {backgroundColor: document_type === '' ? '#EEE' : '#FFF'}]}
              underlineColorAndroid='transparent'
              onChangeText={(document) => updateUserProp({document})}
              value={document}
              editable={!(document_type === '')}
            />
            <Text style={styles.label}>Sobre mim:</Text>
            <TextInput
              style={[
                styles.textInput,
                {
                  color: '#555',
                  fontSize: 13,
                  textAlignVertical: 'top',
                  textAlign: 'center',
                  paddingTop: 5,
                  borderWidth: 1,
                  borderColor: '#B54D80',
                  borderRadius: 10,
                  marginTop: 10,
                  height: 120,
                },
              ]}
              underlineColorAndroid='transparent'
              multiline={true}
              numberOfLines={8}
              value={about}
              onChangeText={about => updateUserProp({about})}
            />
            <Text style={styles.label}>Preferências em:</Text>
            <MultiSelection
              values={preferences}
              onChange={(preferences) => updateUserProp({preferences})}
              items={[
                {
                  label: 'Mulheres',
                  value: 'mulheres',
                },
                {
                  label: 'Homens',
                  value: 'homens',
                },
                {
                  label: 'Outros',
                  value: 'outros',
                },
              ]}>
            </MultiSelection>
            <Text style={styles.label}>Seus objetivos são:</Text>
            <MultiSelection
              values={goals}
              onChange={(goals) => updateUserProp({goals})}
              items={[
                {
                  label: 'Negócios',
                  value: 'negocios',
                },
                {
                  label: 'Relacionamentos',
                  value: 'relacionamentos',
                },
                {
                  label: 'Networking',
                  value: 'networking',
                },
                {
                  label: 'Amizades',
                  value: 'amizades',
                },
                {
                  label: 'Outros',
                  value: 'outros',
                },
              ]}>
            </MultiSelection>
            <Text style={styles.label}>Não tem interesse em:</Text>
            <MultiSelection
              values={no_interests}
              onChange={(no_interests) => {
                console.log('no_interests-MultiSelection', no_interests);
                updateUserProp({no_interests});
              }}
              items={[
                {
                  label: 'Fumantes',
                  value: 'fumantes',
                },
                {
                  label: 'Bebidas Alcoólicas',
                  value: 'bebidas_alcoolicas',
                },
                {
                  label: 'Night',
                  value: 'night',
                },
                {
                  label: 'Fitness',
                  value: 'fitness',
                },
                {
                  label: 'Encontros casuais',
                  value: 'encontros_casuais',
                },
              ]}>
            </MultiSelection>
            <Text style={styles.label}>Desejo encontrar pessoas em um raio de:</Text>
            <Slider
              style={{marginTop: 10}}
              step={1}
              onValueChange={(sliderShowValue) => this.setState({sliderShowValue})}
              onSlidingComplete={(radius_around) => updateUserProp({
                radius_around,
              }).then(() => this.setState({sliderShowValue: radius_around}))}
              value={parseInt(radius_around)}
              maximumValue={5000}
              minimumValue={1}
              thumbTintColor='#B54D80'
              maximumTrackTintColor='#B54D80'
              minimumTrackTintColor='#B54D80'
            />
            <View style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
              <Text style={{color: '#000'}}>1km</Text>
              <Text style={{
                color: '#B54D80',
                fontWeight: 'bold',
              }}>{sliderShowValue}</Text>
              <Text style={{color: '#000'}}>5000km</Text>
            </View>
          </View>
          <Button
            containerStyle={{
              backgroundColor: '#7a1043',
              borderWidth: 0,
              borderRadius: 0,
            }}
            onPress={this._userRegister}
            text='Finalizar'
          />
          <Modal
            transparent={false}
            visible={isBusinessSectorVisible}
            onRequestClose={() => {
            }}
          >
            <View style={{
              flex: 1,
              backgroundColor: 'FFF',
            }}>
              <TouchableOpacity onPress={() => this.setState({isBusinessSectorVisible: false})}>
                <View style={{
                  padding: 15,
                  paddingBottom: 5,
                  paddingRight: 10,
                }}>
                  <Icon name="close" size={30} color={'#7a1043'}/>
                </View>
              </TouchableOpacity>
              <FlatList
                data={sectorList}
                keyExtractor={(item, index) => index.toString()}
                renderItem={
                  ({item}) => {
                    return (
                      <TouchableOpacity onPress={() => this.setState({
                          isBusinessSectorVisible: false,
                          business_sector_text: item.name,
                        }, () => updateUserProp({
                          business_sector: item.id,
                        }),
                      )}>
                        <View style={{
                          paddingLeft: 25,
                          justifyContent: 'center',
                          height: 50,
                          borderBottomWidth: 1,
                          borderColor: '#7a1043',
                        }}>
                          <Text>{item.name}</Text>
                        </View>
                      </TouchableOpacity>
                    );
                  }
                }
              />
            </View>
          </Modal>
          <LoaderScreen
            isVisible={isLoading}
            message={loaderMessage}
          />
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({user, sectorList}) => {
  return {
    user,
    sectorList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userRegister: (user, token) => dispatch(userRegister(user, token)),
    fetchSectorList: () => dispatch(fetchSectorList()),
    fetchCoordsByCity: (city, state) => dispatch(fetchCoordsByCity(city, state)),
    updateUserProp: (user) => dispatch(updateUserProp(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SecondSignUp);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  label: {
    color: '#7D7474',
    fontSize: 14,
    marginTop: 15,
  },
  textInput: {
    padding: 0,
    paddingLeft: 15,
    color: '#000',
    fontSize: 15,
    borderBottomWidth: 1,
    borderColor: '#B54D80',
    height: 40,
  },
});
