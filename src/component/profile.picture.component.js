import React, {Component} from 'react';
import { Text, View, TouchableOpacity, TouchableNativeFeedback, TouchableWithoutFeedback, Image, Alert } from 'react-native';
// import ImagePicker from 'react-native-image-picker';
import Icon from "react-native-vector-icons/Entypo";
import ImagePicker from "react-native-image-crop-picker";

class ProfilePicture extends Component {

  constructor(props) {
    super(props);

    this.state = {
      avatarSource: null,
    }
  }

  componentDidMount() {
    // this.setState({avatarSource: this.props.item !== '' ? this.props.item : null});
  }

  _deletePhoto = () => {
      Alert.alert(
          'Deletar!',
          'Deseja remover essa photo de perfil?',
          [
              {
                  text: 'Cancelar',
                  onPress: () => {
                  },
                  style: 'cancel',
              },
              {
                  text: 'OK',
                  onPress: () => {
                    const {removePicture, index} = this.props;
                      removePicture(index);
                  },
              },
          ],
          {cancelable: false},
      );
  };

  _newPicture = () => ImagePicker.openPicker({
    width: 300,
    height: 400,
    cropping: true,
    includeBase64: true,
  }).then(image => {
    console.log(image);
    this.props.selectPicture(`data:image/jpeg;base64,${image.data}`, this.props.index);
  });

  render() {
    return (
      <View style={{marginRight: 5}}>
          {this.props.item !== null && this.props.item.picture ?
              <View>
                <Image source={{uri: this.props.item.picture}} style={{width: 120, height: 120, borderRadius: 10,}} />
                <TouchableWithoutFeedback onPress={this._deletePhoto}>
                  <View style={{
                    backgroundColor: 'rgba(0, 0, 0, 0.6)',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 5,
                    borderRadius: 10,
                    position: 'absolute',
                    bottom: 0,
                    left: 0,
                    right: 0,
                    zIndex: 23,
                  }}>
                    <Text style={{fontSize: 12, color: '#FFF', fontWeight: 'bold'}}>remover</Text>
                </View>
                </TouchableWithoutFeedback>
              </View>
              :
              <TouchableOpacity onPress={() => {
                this._newPicture();
            /*const options = {
              title: 'Select Avatar',
              customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
              storageOptions: {
                skipBackup: true,
                path: 'images',
              },
            };

            /*ImagePicker.launchImageLibrary(options, (response) => {
              console.log('Response = ', response);

              if (response.didCancel) {
                console.log('User cancelled image picker');
              } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
              } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
              } else {
                // const source = {uri: response.uri};

                // You can also display the image using data:
                const source = { uri: 'data:image/jpeg;base64,' + response.data };

                // this.setState({
                //   avatarSource: source.uri,
                // });

                this.props.selectPicture(source.uri);

                Image.getSize('data:image/jpeg;base64,' + response.data, (width,height) => {
                  console.log('width', width);
                  console.log('height', height);
                }, (error) => console.log('erro ao carregar imagem', error));
              }
            });*/
          }}>
            <View style={{
              height: 120,
              width: 120,
              backgroundColor: '#E9B2CD',
              borderRadius: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
                <Text style={{fontSize: 50, color: '#B54D80', fontWeight: 'bold'}}>+</Text>
          </View>
        </TouchableOpacity>
          }
      </View>
    );
  }
}

export default ProfilePicture;
