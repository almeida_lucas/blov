import React from 'react';
import { Text, Modal, View, ActivityIndicator } from 'react-native';

const LoaderScreen = ({message, isVisible, textColor}) => {
  return (
    <Modal
      transparent={true}
      visible={isVisible ? isVisible : false}
      onRequestClose={() => {
      }}
    >
      <View style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
      }}>
          <ActivityIndicator size="large" color="#fff"/>
          <Text style={{color: textColor ? textColor : '#fff', fontSize: 18, marginTop: 15}}>{message}</Text>
      </View>
    </Modal>
  );
};

export default LoaderScreen;
