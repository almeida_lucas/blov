/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @format
 * @flow weak
 */

'use strict';

var React = require('react');
var createReactClass = require('create-react-class');
var ReactNative = require('react-native');
var {PanResponder, StyleSheet, View, Dimensions, Image} = ReactNative;

var CIRCLE_SIZE = 80;

var fullWidth = Dimensions.get('window').width;

var PanResponderExample = createReactClass({
  displayName: 'PanResponderExample',

  statics: {
    title: 'PanResponder Sample',
    description:
      'Shows the use of PanResponder to provide basic gesture handling.',
  },

  _panResponder: {},
  _previousRight: 0,
  _previousLeft: 0,
  _previousBottom: 0,
  _previousHeight: 110,
  _initialHeight: 110,
  _circleStyles: {},
  circle: (null: ?{ setNativeProps(props: Object): void }),

  UNSAFE_componentWillMount: function () {
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: this._handleStartShouldSetPanResponder,
      onMoveShouldSetPanResponder: this._handleMoveShouldSetPanResponder,
      onPanResponderGrant: this._handlePanResponderGrant,
      onPanResponderMove: this._handlePanResponderMove,
      onPanResponderRelease: this._handlePanResponderEnd,
      onPanResponderTerminate: this._handlePanResponderEnd,
    });
    this._previousRight = 0;
    this._previousLeft = 0;
    this._previousBottom = 0;
    this._circleStyles = {
      style: {
        bottom: this._previousBottom,
        height: this._previousHeight,
      },
    };
  },

  componentDidMount: function () {
    this._updateNativeStyles();
  },

  render: function () {
    return (
        <View
          ref={circle => {
            this.circle = circle;
          }}
          style={[styles.circle, {flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'center',}]}
          {...this._panResponder.panHandlers}
        >
          <Image style={{width: 60, height: 60, marginTop: 15,}} source={require('../resource/partner.png')}/>
        </View>
    );
  },

  _highlight: function () {
    // this._circleStyles.style.backgroundColor = 'blue';
    // this._updateNativeStyles();
  },

  _unHighlight: function () {
    // this._circleStyles.style.backgroundColor = 'green';
    // this._updateNativeStyles();
  },

  _updateNativeStyles: function () {
    this.circle && this.circle.setNativeProps(this._circleStyles);
  },

  _handleStartShouldSetPanResponder: function (
    e: Object,
    gestureState: Object,
  ): boolean {
    // Should we become active when the user presses down on the circle?
    return true;
  },

  _handleMoveShouldSetPanResponder: function (
    e: Object,
    gestureState: Object,
  ): boolean {
    // Should we become active when the user moves a touch over the circle?
    return true;
  },

  _handlePanResponderGrant: function (e: Object, gestureState: Object) {
    // this._highlight();
  },
  _handlePanResponderMove: function (e: Object, gestureState: Object) {
    if (gestureState.dy < 0 && !(this._circleStyles.style.height > (this._initialHeight + 150))) {
      // this._circleStyles.style.left = this._previousLeft + gestureState.dx;
      this._circleStyles.style.height = this._previousHeight + (gestureState.dy * -1);
      this._updateNativeStyles();
      const opacity = (this._circleStyles.style.height * 100) / (this._initialHeight + 150) / 100;
      this.props.changeLike(`rgba(6,90,250,${opacity})`);
    } else if (this._circleStyles.style.height > (this._initialHeight + 150)) {
      if (!this.isFinished) {
        this.isFinished = true;
      }
    }
  },
  _handlePanResponderEnd: function (e: Object, gestureState: Object) {
    // this._unHighlight();
    // this._circleStyles.style.left = this._previousLeft;
    this._circleStyles.style.height = this._previousHeight;
    // this._circleStyles.style.left = this._previousLeft;
    this.isFinished = false;
    this._updateNativeStyles();
    this.props.changeLike(`transparent`);
// this._previousTop += gestureState.dy;
  },
});

var styles = StyleSheet.create({
  circle: {
    backgroundColor: '#107cfb',
    borderTopLeftRadius: 80,
    borderTopRightRadius: 80,
    width: 100,
    alignSelf: 'center',
    position: 'absolute',
    zIndex: 10,
  },
  container: {
    flex: 1,
  },
});

module.exports = PanResponderExample;
