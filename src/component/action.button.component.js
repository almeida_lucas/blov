import React, { Component } from 'react';
import { Image, TouchableWithoutFeedback, View, Animated } from 'react-native';


const AnmView = Animated.createAnimatedComponent(View);
const AnmImage = Animated.createAnimatedComponent(Image);

class ActionButton extends Component {

  _doAction = () => {
    this.props.onActionSelect();
  };

  render() {
    const {image, containerStyle} = this.props;

    return (
      <TouchableWithoutFeedback onPress={this._doAction}>
        <AnmView style={[{
          width: 65,
          height: 65,
          borderRadius: 65,
          zIndex: 0,
        }, containerStyle]}>
          <AnmImage
            source={image}
          />
        </AnmView>
      </TouchableWithoutFeedback>
    );
  }
}

export default ActionButton;
