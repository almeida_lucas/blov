import React from 'react';
import { Image, Text, TouchableOpacity, View, Platform } from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Header = ({filterButton, backButton, componentId}) => {
  return (
    <View style={{
      height: 50,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingRight: 15,
      paddingLeft: 15,
      borderBottomWidth: 1,
      borderColor: '#333',
      backgroundColor: '#fff',
      marginTop: Platform.OS === 'ios' ? 15 : 0,
    }}>
      <TouchableOpacity onPress={() => {
          if (backButton) {
              Navigation.pop(componentId);
          } else {
              Navigation.mergeOptions('Drawer', {
                  sideMenu: {
                      left: {
                          visible: true,
                      },
                  },
              });
          }
      }}>
          {backButton ?
              <Icon name="arrow-back" size={30} color={'#000'}/>
              :
              <Icon name="menu" size={30} color={'#333'}/>
          }
      </TouchableOpacity>
      <Image
        style={{width: 68, height: 25}}
        source={require('../resource/logo-title.png')}
      />
      {filterButton ?
        <TouchableOpacity onPress={filterButton.onPress}>
          <Image style={{width: 30, height: 30,}} source={require('../resource/filter.png')}/>
        </TouchableOpacity>
        :
        <View style={{width: 30}}/>
      }
    </View>
  );
};

export default Header;
