import React, { Component } from 'react';
import { Text, View } from 'react-native';

class CustomMarker extends Component {


  render() {
    return (
      <View>
        <View style={{marginTop: 15, backgroundColor: '#fff', width: 20, height: 20, borderWidth: 1, borderColor: '#7a1043', borderRadius: 20,}}/>
        <Text style={{fontSize: 12, color: '#7a1043', alignSelf: 'center',}}>{this.props.text}</Text>
      </View>
    );
  }
}

export default CustomMarker;
