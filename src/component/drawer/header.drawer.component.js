import React from 'react';

import {
  View,
  Text,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {
  Avatar,
  AvatarHelper,
} from 'react-native-ui-lib';
import Icon from 'react-native-vector-icons/MaterialIcons';

const DrawerHeader = ({name, pictures}) => {
  let pictureTemp = '';
  if (pictures && pictures.length > 0 && pictures[0].picture) {
    pictureTemp = {
      title: 'Image (online)',
        imageSource: {
      uri: pictures[0].picture,
    },
      isOnline: true,
    }
  }
  const hasProfilePicture = pictures && pictures.length > 0 && pictures[0].picture;

  return (
    <View style={{
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginBottom: 25,
      marginTop: Platform.OS === 'ios' ? 15 : 0,
    }}>
      <View style={{
        flexDirection: 'row',
        alignItems: 'center',
      }}>
        <View style={{
          borderColor: '#8D2457',
          borderWidth: 2,
          borderRadius: 35,
        }}>
          <Avatar
            imageSource={{uri: hasProfilePicture ? pictures[0].picture : null}}
            size={35}
            label={AvatarHelper.getInitials(name ? name : '')}
            labelColor={'#8D2457'}
          />
        </View>
        <Text style={{
          color: '#8D2457',
          marginLeft: 15,
          fontSize: 14,
        }}>{name ? name : ''}</Text>
      </View>
      <TouchableOpacity onPress={() => Navigation.mergeOptions('Drawer', {
        sideMenu: {
          left: {
            visible: false,
          },
        },
      })}>
        <Icon name="menu" size={25} color={'#8D2457'}/>
      </TouchableOpacity>
    </View>
  );
};

export default DrawerHeader;
