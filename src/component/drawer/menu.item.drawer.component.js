import React from 'react';

import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const MenuItem = ({text, onPress, iconName}) => {
  return (
      <TouchableOpacity onPress={onPress ? onPress : () => {console.warn('TODO')}}>
        <View style={{
          marginTop: 15,
          paddingLeft: 5,
          paddingBottom: 5,
          alignItems: 'stretch',
          borderBottomWidth: 0.5,
          borderColor: '#8D2457',
          flexDirection: 'row',
        }}>
          <Icon name={iconName} size={25} color={'#8D2457'}/>
          <Text style={{color: '#8D2457', fontSize: 16, marginLeft: 15,}}>
            {text}
          </Text>
        </View>
      </TouchableOpacity>
  );
};

export default MenuItem;
