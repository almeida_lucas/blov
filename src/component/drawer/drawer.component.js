import React from 'react';

import {
  View,
} from 'react-native';

import DrawerHeader from './header.drawer.component';
import DrawerMenu from './menu.drawer.component';
import connect from 'react-redux/es/connect/connect';
import {userLogin} from '../../action/user.action';

const Drawer = ({user}) => {
  return (
    <View style={{
      flex: 1,
      backgroundColor: '#FFF',
      alignItems: 'stretch',
      padding: 15,
    }}>
      <DrawerHeader {...user}/>
      <DrawerMenu/>
    </View>
  );
};

const mapStateToProps = ({user}) => {
  return {
    user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userLogin: () => dispatch(userLogin()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Drawer);
