import React from 'react';

import { View, AsyncStorage } from 'react-native';
import MenuItem from './menu.item.drawer.component';
import { Navigation } from 'react-native-navigation';
import { LoginManager } from 'react-native-fbsdk';

const DrawerMenu = () => {
  return (
    <View style={{flex: 1, justifyContent: 'flex-start',}}>
      <MenuItem
        text='Editar perfil'
        onPress={() => {
          Navigation.mergeOptions('Drawer', {
            sideMenu: {
              left: {
                visible: false,
              },
            },
          });
          Navigation.push('StackScreens', {
            component: {
              name: 'FirstSignUp',
              passProps: {
                isEditProfile: true,
              },
            }
          })
        }}
        iconName={'account-edit'}
      />
      <MenuItem
        text='Meu plano'
        onPress={() => {
            Navigation.push('StackScreens', {
                component: {
                    name: 'MyPlanScreen',
                }
            })
        }}
        iconName={'coin'}
      />
      <MenuItem
        text='Sair'
        onPress={async () => {
          await LoginManager.logOut();
          await AsyncStorage.clear();
          Navigation.setRoot({
            root: {
              stack: {
                children: [
                  {
                    component: {
                      id: 'LoginScreen',
                      name: 'LoginScreen',
                    },
                  },
                ],
              },
            },
          });
        }}
        iconName={'logout'}
      />
    </View>
  );
};

export default DrawerMenu;
