import React, { PureComponent } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

class MultiSelectionItem extends PureComponent {

  _onChangeItem = () => {
    this.props.onPress(this.props.isSelected, this.props.value);
  };

  render() {
    const {label, isSelected, isDisabled} = this.props;
    const backgroundColor = isSelected ? '#B54D80' : '#FFF';
    const color = isSelected ? '#FFF' : '#B54D80';

    return (
      <TouchableOpacity onPress={this._onChangeItem} disabled={isDisabled ? isDisabled : false}>
        <View style={{
          marginTop: 10,
          borderRadius: 10,
          padding: 5,
          borderWidth: 1,
          borderColor: isSelected ? 'transparent' : '#B54D80',
          backgroundColor,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
          <Icon name="plus" size={8} color={color}/>
          <Text style={{marginLeft: 5, color, fontSize: 11}}>
            {label}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

class MultiSelection extends PureComponent {

  _onChangeValue = (isSelected, value) => {
    const {values, onChange} = this.props;
    let selectedItems = [];
    if (isSelected)
      selectedItems = values.filter(item => item !== value);
    else
      selectedItems = [...values, value];

    onChange(selectedItems);
  };

  render() {
    const {values, isDisabled, items} = this.props;
    return (
      <View style={{
        flexWrap: 'wrap',
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
      }}>
        {
            items.map((item, index) => {
            const isSelected = values.find(value => value === item.value) !== undefined;
            return (
              <MultiSelectionItem isDisabled={isDisabled} isSelected={isSelected} onPress={this._onChangeValue} {...item} key={index}/>
            );
          })
        }
      </View>
    );
  }
}

export default MultiSelection;
