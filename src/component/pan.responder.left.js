/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @format
 * @flow weak
 */

'use strict';

var React = require('react');
var createReactClass = require('create-react-class');
var ReactNative = require('react-native');
var {PanResponder, StyleSheet, View, Image} = ReactNative;

var CIRCLE_SIZE = 80;

var PanResponderExample = createReactClass({
  displayName: 'PanResponderExample',

  statics: {
    title: 'PanResponder Sample',
    description:
      'Shows the use of PanResponder to provide basic gesture handling.',
  },

  _panResponder: {},
  _previousLeft: 0,
  _previousTop: 0,
  _previousWidth: 110,
  _initialWidth: 110,
  _circleStyles: {},
  circle: (null: ?{ setNativeProps(props: Object): void }),

  UNSAFE_componentWillMount: function () {
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: this._handleStartShouldSetPanResponder,
      onMoveShouldSetPanResponder: this._handleMoveShouldSetPanResponder,
      onPanResponderGrant: this._handlePanResponderGrant,
      onPanResponderMove: this._handlePanResponderMove,
      onPanResponderRelease: this._handlePanResponderEnd,
      onPanResponderTerminate: this._handlePanResponderEnd,
    });
    this._previousLeft = 0;
    this._previousTop = 0;
    this._circleStyles = {
      style: {
        left: this._previousLeft,
        top: this._previousTop,
        width: this._previousWidth,
      },
    };
  },

  componentDidMount: function () {
    this._updateNativeStyles();
  },

  render: function () {
    return (
      <View
        ref={circle => {
          this.circle = circle;
        }}
        style={[styles.circle, {flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'}]}
        {...this._panResponder.panHandlers}
      >
        <Image style={{width: 60, height: 60, marginRight: 15}} source={require('../resource/dislike.png')}/>
      </View>
    );
  },

  _highlight: function () {
    // this._circleStyles.style.backgroundColor = 'blue';
    // this._updateNativeStyles();
  },

  _unHighlight: function () {
    // this._circleStyles.style.backgroundColor = 'green';
    // this._updateNativeStyles();
  },

  _updateNativeStyles: function () {
    this.circle && this.circle.setNativeProps(this._circleStyles);
  },

  _handleStartShouldSetPanResponder: function (
    e: Object,
    gestureState: Object,
  ): boolean {
    // Should we become active when the user presses down on the circle?
    return true;
  },

  _handleMoveShouldSetPanResponder: function (
    e: Object,
    gestureState: Object,
  ): boolean {
    // Should we become active when the user moves a touch over the circle?
    return true;
  },

  _handlePanResponderGrant: function (e: Object, gestureState: Object) {
    // this._highlight();
  },
  _handlePanResponderMove: function (e: Object, gestureState: Object) {
    if (gestureState.dx > this._previousLeft && !(this._circleStyles.style.width > (this._initialWidth + 150))) {
      // this._circleStyles.style.left = this._previousLeft + gestureState.dx;
      this._circleStyles.style.width = this._previousWidth + (gestureState.dx - this._previousLeft);
      this._updateNativeStyles();
      const opacity = (this._circleStyles.style.width * 100) / (this._initialWidth + 150) / 100;
      this.props.changeLike(`rgba(46,46,46,${opacity})`);
    } else if (this._circleStyles.style.width > (this._initialWidth + 150)) {
      if (!this.isFinished) {
        this.isFinished = true;
      }
    }
  },
  _handlePanResponderEnd: function (e: Object, gestureState: Object) {
    // this._unHighlight();
    // this._circleStyles.style.left = this._previousLeft;
    this._circleStyles.style.width = this._previousWidth;
    this.isFinished = false;
    this._updateNativeStyles();
    const opacity = (this._circleStyles.style.width * 100) / (this._initialWidth + 150) / 100;
    this.props.changeLike(`transparent`);
// this._previousTop += gestureState.dy;
  },
});

var styles = StyleSheet.create({
  circle: {
    backgroundColor: '#777',
    borderTopRightRadius: 80,
    borderBottomRightRadius: 80,
    height: 90,
    position: 'absolute',
  },
  container: {
    flex: 1,
  },
});

module.exports = PanResponderExample;
