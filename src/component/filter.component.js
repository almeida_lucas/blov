import React, { PureComponent } from 'react';
import {View, Text, StyleSheet, Slider, Animated, Image, TouchableOpacity, FlatList, Modal} from 'react-native';
import MultiSelection from './multi.selection.component';
import { Picker } from 'react-native-ui-lib';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import dropdown from '../resource/chevronDown.png';

import CustomMarker from '../component/custom.marker.component';
import Button from '../component/button.component';
import Icon from "react-native-vector-icons/EvilIcons";

const optionsBusinessSector = [
  {label: 'Tecnologia', value: 'T'},
  {label: 'Finanças', value: 'F'},
];
const AnmView = Animated.createAnimatedComponent(View);

class Filter extends PureComponent {

  _startAnimation = () => {
    Animated.timing(this._animatedValue, {
      toValue: 1,
      duration: 500,
    }).start();
  };

  _endAnimation = () => {
    Animated.timing(this._animatedValue, {
      toValue: 0,
      duration: 500,
    }).start();
  };

  constructor(props) {
    super(props);

    const {goals, radius_around, business_sector, sectorList} = props;

    this._animatedValue = new Animated.Value(0);

    const sector = sectorList.find(item => item.id === business_sector);

    this.state = {
      goals,
      business_sector,
      business_sector_text: sector ? sector.name : '',
      radius_around: radius_around ? parseInt(radius_around) : 1,
      sliderShowValue: radius_around ? parseInt(radius_around) : 1,
      multiSliderValue: [18, 60],
        isBusinessSectorVisible: false,
    };
  }

  componentDidUpdate(prevProps, prevState): void {
      const {goals, radius_around, business_sector, sectorList} = this.props;
      const sector = sectorList.find(item => item.name === business_sector);
      if (sector && this.state.business_sector_text === '')
          this.setState({business_sector_text: sector.name, business_sector: sector.id});
  }

    render() {
    const {radius_around, sliderShowValue, goals, business_sector, business_sector_text, multiSliderValue, isBusinessSectorVisible} = this.state;
    const {search, token, sectorList} = this.props;

    return (
      <AnmView
        style={{
          position: 'absolute',
          top: 50,
          right: 0,
          left: 0,
          backgroundColor: '#fff',
          alignItems: 'stretch',
          paddingLeft: 15,
          paddingRight: 15,
          zIndex: this._animatedValue.interpolate({
              inputRange: [0, 1],
              outputRange: [-1, 25],
          }),
          height: 440,
            opacity: this._animatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 1],
            }),
        }}>
        <Text style={styles.label}>Desejo encontrar pessoas em um raio de:</Text>
        <Slider
          style={{marginTop: 10}}
          step={1}
          onValueChange={(sliderShowValue) => this.setState({sliderShowValue})}
          onSlidingComplete={(radius_around) => this.setState({radius_around, sliderShowValue: radius_around})}
          value={radius_around}
          maximumValue={5000}
          minimumValue={1}
          thumbTintColor='#B54D80'
          maximumTrackTintColor='#B54D80'
          minimumTrackTintColor='#B54D80'
        />
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={{color: '#000'}}>1km</Text>
          <Text style={{color: '#B54D80', fontWeight: 'bold'}}>{sliderShowValue}</Text>
          <Text style={{color: '#000'}}>5000km</Text>
        </View>
        <Text style={styles.label}>Interesses em:</Text>
        <MultiSelection
          values={goals}
          onChange={(goals) => this.setState({goals})}
          items={[
            {label: 'Negócios', value: 'negocios'},
            {label: 'Relacionamentos', value: 'relacionamentos'},
            {label: 'Networking', value: 'networking'},
            {label: 'Amizades', value: 'amizades'},
            {label: 'Outros', value: 'outros'},
          ]}>
        </MultiSelection>
        <Text style={styles.label}>Setor que empreende:</Text>
          <TouchableOpacity onPress={() => this.setState({isBusinessSectorVisible: true})}>
              <View style={{
                  height: 40,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  borderBottomWidth: 1,
                  borderColor: '#B54D80',
              }}>
                  <Text>{business_sector_text ? business_sector_text : ''}</Text>
                  <Image
                      style={{width: 14, height: 8}}
                      source={dropdown}
                  />
              </View>
          </TouchableOpacity>
        <Text style={styles.label}>Idade:</Text>
        <MultiSlider
          containerStyle={{justifyContent: 'center', alignItems: 'center', marginBottom: 10}}
          sliderLength={280}
          selectedStyle={{height: 8, marginTop: -4, backgroundColor: '#B54D80'}}
          values={[this.state.multiSliderValue[0], this.state.multiSliderValue[1]]}
          onValuesChangeFinish={(multiSliderValue) => this.setState({multiSliderValue})}
          min={18}
          max={50}
          step={1}
          isMarkersSeparated={true}
          customMarkerLeft={(e) => {
            return (<CustomMarker
              text={e.currentValue}/>);
          }}

          customMarkerRight={(e) => {
            return (<CustomMarker
                text={e.currentValue}/>
            );
          }}
        />
        <Button
          onPress={() => {
            search(token, {
              distance: radius_around,
              interests: goals,
              business_sector,
              age_start: multiSliderValue[0],
              age_end: multiSliderValue[1],
            });
            this._endAnimation();
          }}
          text='Pesquisar'
          icon='magnifying-glass'
          containerStyle={{
            backgroundColor: '#B54D80',
            justifyContent: 'space-evenly',
            width: 140,
            borderRadius: 15,
            alignSelf: 'flex-end',
          }}
        />
          <Modal
              transparent={false}
              visible={isBusinessSectorVisible}
              onRequestClose={() => {
              }}
          >
              <View style={{
                  flex: 1,
                  backgroundColor: 'FFF',
              }}>
                  <TouchableOpacity onPress={() => this.setState({isBusinessSectorVisible: false})}>
                      <View style={{padding: 15, paddingBottom: 5, paddingRight: 10,}}>
                          <Icon name="close" size={30} color={'#B54D80'}/>
                      </View>
                  </TouchableOpacity>
                  <FlatList
                      data={sectorList}
                      keyExtractor={(item, index) => index.toString()}
                      renderItem={
                          ({item}) => {
                              return (
                                  <TouchableOpacity onPress={() => this.setState({business_sector_text: item.name, business_sector: item.id, isBusinessSectorVisible: false})}>
                                      <View style={{paddingLeft: 25, justifyContent: 'center', height: 50, borderBottomWidth: 1, borderColor: '#B54D80'}}>
                                          <Text>{item.name}</Text>
                                      </View>
                                  </TouchableOpacity>
                              );
                          }
                      }
                  />
              </View>
          </Modal>
      </AnmView>
    );
  }
}

export default Filter;

const styles = StyleSheet.create({
  label: {
    color: '#999',
    fontSize: 14,
    marginTop: 15,
  },
});
