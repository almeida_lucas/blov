import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from "react-native";
import Avatar from "react-native-ui-lib/typings/components/avatar";
import * as AvatarHelper from "react-native-ui-lib/typings/helpers/AvatarHelper";
import {Navigation} from "react-native-navigation";


class SolicitItem extends Component {

    constructor() {
        super();

        this.state = {
            isMatchAccepted: false,
        }
    }

    _showChatScreen = () => {
        Navigation.mergeOptions('BottomTabs', {
            bottomTabs: {
                currentTabIndex: 2
            }
        });
        /*Navigation.showModal({
            component: {
                id: 'ChatItemScreen',
                name: 'ChatItemScreen',
                options: {
                    screenBackgroundColor: 'transparent',
                    modalPresentationStyle: 'overCurrentContext',
                },
            },
        });*/
        this.props.removeItem(this.props.index);
    };

    _removeItem = () => {
        const {match, confirmMatch, id, removeItem, index} = this.props;
        confirmMatch(id.toString(), match.toString());
        removeItem(index);
    };

    _confirmSolicitation = () => {
        const {match, confirmMatch, id, user_token} = this.props;
        confirmMatch(id.toString(), match.toString(), user_token);
    };

    render() {
        const {isMatchAccepted} = this.state;
        const {name, occupation, age, match} = this.props;
        return(
            <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                borderBottomWidth: 1,
                borderColor: '#CCC',
            }}>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center',
                margin: 15,
                marginTop: 10,
                marginBottom: 10,
                }}>
                    <Avatar
                        label={AvatarHelper.getInitials(name)}
                        backgroundColor={'#dc9dbc'}
                        labelColor={'#222'}
                        size={50}
                    />
                    <View style={{marginLeft: 10}}>
                        <Text style={{color: '#333', fontSize: 16, fontWeight: 'bold'}}>{name}</Text>
                        <Text style={{color: '#777', fontSize: 14}}>{age} anos, {occupation}</Text>
                    </View>
                </View>
                {isMatchAccepted ?
                    <TouchableOpacity onPress={this._showChatScreen}>
                        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: 95, height: 70, backgroundColor: '#107cfb'}}>
                            <Image style={{width: 30, height: 30}} source={require('../resource/chat_solicit.png')}/>
                        </View>
                    </TouchableOpacity>
                    :
                    <View style={{flexDirection: 'row', margin: 15,
                        marginTop: 10,
                        marginBottom: 10,}}>
                            <TouchableOpacity onPress={() => {
                                this.setState({isMatchAccepted: true});
                                this._confirmSolicitation();
                            }}>
                                <View style={{backgroundColor: match.toString() !== '1' ? '#107cfb' : '#ff4e4e', borderRadius: 30, padding: 6, marginRight: 10}}>
                                    {match.toString() !== '1' ?
                                        <Image style={{width: 25, height: 25}} source={require('../resource/partner.png')}/>
                                        :
                                        <Image style={{width: 25, height: 25}} source={require('../resource/like.png')}/>
                                    }
                                </View>
                            </TouchableOpacity>
                        <TouchableOpacity onPress={this._removeItem}>
                            <View style={{backgroundColor: '#777', borderRadius: 30, padding: 6}}>
                                <Image style={{width: 25, height: 25}} source={require('../resource/dislike.png')}/>
                            </View>
                        </TouchableOpacity>
                    </View>
                }
            </View>
        );
    }
}

export default SolicitItem;