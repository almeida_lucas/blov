import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

const Button = ({onPress, text, containerStyle = {}, icon}) => {
  return (
    <View style={{
      alignSelf: 'stretch',
      alignItems: 'stretch',
    }}>
      <TouchableOpacity onPress={onPress}>
        <View style={[
          {
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            height: 50,
            borderWidth: 1,
            borderRadius: 5,
            borderColor: '#fff',
          },
          {...containerStyle},
        ]}>
          {icon ?
            <Icon name={icon} size={20} color={'#fff'}/>
            :
            null
          }
          <Text style={{fontSize: 16, color: '#fff', textAlign: 'center'}}>{text}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Button;
