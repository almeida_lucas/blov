import {AccessToken, GraphRequest, GraphRequestManager, LoginManager} from "react-native-fbsdk";

export const facebookLogin = async (updateStateAndRedirect) => {
    // native_only config will fail in the case that the user has
    // not installed in his device the Facebook app. In this case we
    // need to go for webview.
    await LoginManager.logOut();
    let result;
    try {
        // this.setState({showLoadingModal: true});
        LoginManager.setLoginBehavior('NATIVE_ONLY');
        result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
    } catch (nativeError) {
        try {
            LoginManager.setLoginBehavior('WEB_ONLY');
            result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
        } catch (webError) {
            // show error message to the user if none of the FB screens
            // did not open
        }
    }

// handle the case that users clicks cancel button in Login view
    if (result.isCancelled) {
        console.log('cancelou', result);
        // this.setState({
        //   showLoadingModal: false,
        //   notificationMessage: I18n.t('welcome.FACEBOOK_CANCEL_LOGIN')
        // });
    } else {
        // Create a graph request asking for user information
        const accessData = await AccessToken.getCurrentAccessToken();

        // Create a graph request asking for user information
        const infoRequest = new GraphRequest('/me', {
            accessToken: accessData.accessToken,
            parameters: {
                fields: {
                    string: 'id, email, picture, first_name, last_name',
                },
            },
        }, (error, result) => {
            console.log('token', accessData.accessToken);
            console.log('error - FBLoginCallback', error);
            console.log('result - FBLoginCallback', result);

            updateStateAndRedirect(accessData.accessToken, 'facebook');
        });

        // Execute the graph request created above
        new GraphRequestManager().addRequest(infoRequest).start();
    }
};