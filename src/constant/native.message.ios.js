import {AlertIOS} from 'react-native';

const showMessage = (title, message, okCallback) => {
  AlertIOS.alert(title, message, [
    {
      text: 'OK',
      onPress: okCallback ? okCallback : () => {},
      style: 'default',
    }
  ])
};

export default showMessage;