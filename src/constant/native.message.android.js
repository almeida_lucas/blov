import {ToastAndroid} from 'react-native';

const showMessage = (title, message) => {
  ToastAndroid.show(message, ToastAndroid.SHORT);
};

export default showMessage;