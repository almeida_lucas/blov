import {Navigation} from 'react-native-navigation';

const InitTabNavigation = () =>
  Navigation.setRoot({
    root: {
      stack: {
        id: 'StackScreens',
        children: [
          {
            sideMenu: {
              left: {
                component: {
                  id: 'Drawer',
                  name: 'Drawer',
                  options: {
                    topBar: {
                      visible: false,
                      drawBehind: true,
                      animate: false,
                    },
                  },
                },
              },
              center: {
                bottomTabs: {
                  id: 'BottomTabs',
                  name: 'BottomTabs',
                  options: {
                    bottomTabs: {
                      backgroundColor: '#FFF',
                      // currentTabIndex: 0,
                    },
                  },
                  children: [
                    {
                      component: {
                        id: 'SearchScreen',
                        name: 'SearchScreen',
                        options: {
                          bottomTab: {
                            text: 'Buscar',
                            icon: require('../resource/search-icon.png'),
                            iconColor: '#B54D80',
                            selectedIconColor: '#B54D80',
                            textColor: '#B54D80',
                            selectedTextColor: '#B54D80',
                            fontFamily: 'Helvetica',
                            fontSize: 10,
                          },
                        },
                      },
                    },
                    {
                      component: {
                        id: 'SolicitScreen',
                        name: 'SolicitScreen',
                        options: {
                          bottomTab: {
                            text: 'Solicitações',
                            icon: require('../resource/bell-ring-icon.png'),
                            iconColor: '#B54D80',
                            selectedIconColor: '#B54D80',
                            textColor: '#B54D80',
                            selectedTextColor: '#B54D80',
                            fontFamily: 'Helvetica',
                            fontSize: 10,
                          },
                        },
                      },
                    },
                    {
                      component: {
                        id: 'ChatScreen',
                        name: 'ChatScreen',
                        options: {
                          bottomTab: {
                            text: 'Chat',
                            icon: require('../resource/chat-icon.png'),
                            iconColor: '#B54D80',
                            selectedIconColor: '#B54D80',
                            textColor: '#B54D80',
                            selectedTextColor: '#B54D80',
                            fontFamily: 'Helvetica',
                            fontSize: 10,
                          },
                        },
                      },
                    },
                  ],
                },
              },
            },
          },
        ],
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false,
          },
        },
      },
    },
  });

export default InitTabNavigation;
