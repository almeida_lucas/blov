import { FETCH_CITY_COORDS_SUCCESS, FETCH_CITY_COORDS_ERROR } from '../constant';

export default (state = null, action = null) => {
  const {type, data} = action;
  switch (type) {
    case FETCH_CITY_COORDS_SUCCESS:
      return data;
    case FETCH_CITY_COORDS_ERROR:
    default:
      return state;
  }
}
