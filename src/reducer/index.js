import { combineReducers } from 'redux';
import user from './user.reducer';
import userList from './user.list.reducer';
import cities from './cities.reducer';
import matchesList from './matches.reducer';
import chatList from './chat.list.reducer';
import chatMessages from './chat.messages.reducer';
import sectorList from './sectors.reducer';
import coords from './coordinates.reducer';

export default combineReducers({
  user,
  userList,
    cities,
    matchesList,
    chatList,
    chatMessages,
  sectorList,
  coords,
});
