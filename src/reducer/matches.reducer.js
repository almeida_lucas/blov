import {FETCH_MATCHES_SUCCESS, FETCH_MATCHES_ERROR, CONFIRM_MATCHES_SUCCESS, CONFIRM_MATCHES_ERROR} from '../constant';

export default (state = { matches: [], approved_matches: [] }, action = null) => {
  const {type, data} = action;
  switch (type) {
    case FETCH_MATCHES_SUCCESS:
      return data;
    case FETCH_MATCHES_ERROR:
      case CONFIRM_MATCHES_SUCCESS:
      case CONFIRM_MATCHES_ERROR:
    default:
      return state;
  }
}
