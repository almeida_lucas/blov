import { LOGIN_ERROR, LOGIN_SUCCESS } from '../constant';

export default (state = {name: '', email: ''}, action = null) => {
  const {type, data} = action;
  switch (type) {
    case LOGIN_SUCCESS:
      return data;
    case LOGIN_ERROR:
    default:
      return state;
  }
}
