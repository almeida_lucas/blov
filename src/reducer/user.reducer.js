import {
  FETCH_LOCAL_USER_ERROR,
  FETCH_LOCAL_USER_SUCCESS,
  LOGIN_ERROR,
  LOGIN_SUCCESS,
  REGISTER_ERROR,
  REGISTER_SUCCESS,
  UPDATE_USER,
} from '../constant';

export default (state = {
  id: 0,
  name: '',
  email: '',
  phone: '',
  gender: '',
  birthdate: '',
  height: '',
  city: '',
  state: '',
  marital_status: '',
  schooling: '',
  business_sector: '',
  about: '',
  preferences: [],
  goals: [],
  no_interests: [],
  radius_around: 1,
  pictures: [],
  document: '',
  document_type: '',
  user_token: '',
}, action = null) => {
  const {type, data} = action;
  switch (type) {
    case LOGIN_SUCCESS:
    case REGISTER_SUCCESS:
    case FETCH_LOCAL_USER_SUCCESS:
      return data;
    case UPDATE_USER:
      return {
        ...state,
        ...data,
      };
    case LOGIN_ERROR:
    case REGISTER_ERROR:
    case FETCH_LOCAL_USER_ERROR:
    default:
      return state;
  }
}
