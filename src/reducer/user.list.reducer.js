import { USER_LIST_ERROR, USER_LIST_SUCCESS, USER_MATCH_SUCCESS, USER_MATCH_ERROR } from '../constant';

export default (state = [], action = null) => {
  const {type, data, match} = action;
  switch (type) {
    case USER_LIST_SUCCESS:
      return data;
    case USER_MATCH_SUCCESS:
      return state.filter(user => !match.find(item => item.id === user.id));
    case USER_LIST_ERROR:
    case USER_MATCH_ERROR:
    default:
      return state;
  }
}
