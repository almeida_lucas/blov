import { CHANGE_LIKE } from '../constant';

export default (state = 0, action = null) => {
  const {type, opacity} = action;
  switch (type) {
    case CHANGE_LIKE:
      return opacity;
    default:
      return state;
  }
}
