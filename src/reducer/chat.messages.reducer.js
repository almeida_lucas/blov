import {FETCH_CHAT_MESSAGES_SUCCESS, FETCH_CHAT_MESSAGES_ERROR, NEW_CHAT_MESSAGE_SUCCESS} from '../constant';

export default (state = [], action = null) => {
  const {type, data} = action;
  switch (type) {
    case FETCH_CHAT_MESSAGES_SUCCESS:
      return data;
      case NEW_CHAT_MESSAGE_SUCCESS:
        return [...state, data];
    case FETCH_CHAT_MESSAGES_ERROR:
    default:
      return state;
  }
}
