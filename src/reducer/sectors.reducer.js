import { FETCH_SECTOR_LIST_ERROR, FETCH_SECTOR_LIST_SUCCESS } from '../constant';

export default (state = [], action = null) => {
  const {type, data} = action;
  switch (type) {
    case FETCH_SECTOR_LIST_SUCCESS:
      return data;
    case FETCH_SECTOR_LIST_ERROR:
    default:
      return state;
  }
}
