import {FETCH_CHAT_LIST_ERROR, FETCH_CHAT_LIST_SUCCESS, UPDATE_CHAT_LIST_SUCCESS} from '../constant';

export default (state = [], action = null) => {
  const {type, data, chat_id, message} = action;
  switch (type) {
    case FETCH_CHAT_LIST_SUCCESS:
      return data;
    case UPDATE_CHAT_LIST_SUCCESS:
      return state.map(item => {
          if (item.chat_id.toString() === chat_id.toString()) {
            console.log(`${UPDATE_CHAT_LIST_SUCCESS} - reducer`, item);
            console.log(`${UPDATE_CHAT_LIST_SUCCESS} - reducer`, {
              ...item,
              chat_history: [...item.chat_history, message],
            });
              return {
                ...item,
                chat_history: [...item.chat_history, message],
              };
          } else
            return item;
      });
    case FETCH_CHAT_LIST_ERROR:
    default:
      return state;
  }
}
