import { FETCH_CITIES_ERROR, FETCH_CITIES_SUCCESS } from '../constant';

export default (state = [], action = null) => {
  const {type, data} = action;
  switch (type) {
    case FETCH_CITIES_SUCCESS:
      return data;
    case FETCH_CITIES_ERROR:
    default:
      return state;
  }
}
