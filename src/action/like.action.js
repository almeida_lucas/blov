import { CHANGE_LIKE, } from '../constant';

export const changeLike = async (opacity) => {
  return {
    type: CHANGE_LIKE,
    opacity,
  };
};
