import axios from 'axios';
import { FETCH_CHAT_LIST_SUCCESS, FETCH_CHAT_LIST_ERROR, UPDATE_CHAT_LIST_SUCCESS, URL } from '../constant';
import firebase from "react-native-firebase";

export const fetchChatList = (id, token) => {
  /*return firebase.firestore().collection('chat_messages')
  .doc(id.toString())
  .get()
  .then((querySnapshot) => {
    const chat_list = querySnapshot.data().chat_list;
    console.log(`FETCH_CHAT_LIST_SUCCESS chat_list ->`, chat_list);
    return {
      type: FETCH_CHAT_LIST_SUCCESS,
      data: chat_list ? chat_list : [],
    };
  })
  .catch(message => {
    console.log(FETCH_CHAT_LIST_ERROR, message);
    return {
      type: FETCH_CHAT_LIST_ERROR,
      message: message.toString(),
    };
  });*/


  return axios(`${URL}/api/v1/users/${id}/chat`, {
    method: 'GET',
      headers: {
          'Authorization': `Bearer${token}`,
      },
  }).then(({data}) => {
      console.log(FETCH_CHAT_LIST_SUCCESS, data);
    return {
      type: FETCH_CHAT_LIST_SUCCESS,
      data,
    };
  }).catch(message => {
      console.log(FETCH_CHAT_LIST_ERROR, message);
      return {
      type: FETCH_CHAT_LIST_ERROR,
      message: message.toString(),
    };
  });
};

export const updateChatList = async (chat_id, message) => {
    return {
        type: UPDATE_CHAT_LIST_SUCCESS,
        chat_id,
        message,
    };
};
