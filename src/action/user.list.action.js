import axios from 'axios';
import { URL, USER_LIST_ERROR, USER_LIST_SUCCESS, USER_MATCH_ERROR, USER_MATCH_SUCCESS } from '../constant';

export const fetchUserList = (token) => {
  return axios(`${URL}/api/v1/users/list`, {
    method: 'GET',
    headers: {
      'Authorization': `Bearer${token}`,
    },
  }).then(({data}) => {
    return {
      type: USER_LIST_SUCCESS,
      data,
    };
  }).catch(message => {
    return {
      type: USER_LIST_ERROR,
      message: message.toString(),
    };
  });
};

export const matchRegister = (token, match) => {
  console.log('matchRegister - token', token);
  console.log('matchRegister - match', match);
  return axios(`${URL}/api/v1/matches`, {
    method: 'POST',
    headers: {
      'Authorization': `Bearer${token}`,
    },
      data: {match},
  }).then(({data}) => {
    console.log(USER_MATCH_SUCCESS, data);
    if (data.status)
      return {
        type: USER_MATCH_SUCCESS,
        match,
      };

    return {
      type: USER_MATCH_ERROR,
      message: data.toString(),
    };
  }).catch(message => {
    console.log(USER_MATCH_ERROR, message);
    return {
      type: USER_MATCH_ERROR,
      message: message.toString(),
    };
  });
};

export const filterUserList = (token, data) => {
  return axios(`${URL}/api/v1/users/filter`, {
    method: 'POST',
    headers: {
      'Authorization': `Bearer${token}`,
    },
    data,
  }).then(({data}) => {
    console.log(USER_LIST_SUCCESS, data);
    if (data.status === 'Nenhum resultado encontrado.')
      return {
        type: USER_LIST_ERROR,
        message: data.status,
      };
    return {
      type: USER_LIST_SUCCESS,
      data,
    };
  }).catch(message => {
    console.log(USER_LIST_ERROR, message);
    return {
      type: USER_LIST_ERROR,
      message: message.toString(),
    };
  });
};
