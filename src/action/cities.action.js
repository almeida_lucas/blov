import axios from 'axios';
import { FETCH_CITIES_SUCCESS, FETCH_CITIES_ERROR, URL } from '../constant';

export const fetchCities = (state) => {
  return axios(`${URL}/api/v1/cities/${state}`, {
    method: 'GET',
  }).then(({data}) => {
    return {
      type: FETCH_CITIES_SUCCESS,
      data,
    };
  }).catch(message => {
    return {
      type: FETCH_CITIES_ERROR,
      message: message.toString(),
    };
  });
};
