import axios from 'axios';
import { LOGIN_ERROR, LOGIN_SUCCESS, URL } from '../constant';

export const userLogin = (user, access_token, type_login) => {
  return axios(`${URL}/api/v1/users/auth`, {
    method: 'POST',
    data: {
      type_login,
      access_token,
    },
  }).then((res) => {
    return {
      type: LOGIN_SUCCESS,
      data: user,
    };
  }).catch(message => {
    return {
      type: LOGIN_ERROR,
      message: message.toString(),
    };
  });
};
