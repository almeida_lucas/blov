import axios from 'axios';
import {
    URL,
    FETCH_CHAT_MESSAGES_SUCCESS,
    FETCH_CHAT_MESSAGES_ERROR,
    NEW_CHAT_MESSAGE_SUCCESS,
    NEW_CHAT_MESSAGE_ERROR,
} from '../constant';

export const fetchChatMessages = (chat_id, user_id, token) => {
    // console.log('token', token);
  return axios(`${URL}/api/v1/users/${user_id}/chat/${chat_id}`, {
    method: 'GET',
      headers: {
          'Authorization': `Bearer${token}`,
      },
  }).then(({data}) => {
      console.log(FETCH_CHAT_MESSAGES_SUCCESS, data);
    return {
      type: FETCH_CHAT_MESSAGES_SUCCESS,
      data,
    };
  }).catch(message => {
      console.log(FETCH_CHAT_MESSAGES_ERROR, message);
      return {
      type: FETCH_CHAT_MESSAGES_ERROR,
      message: message.toString(),
    };
  });
};

export const newChatMessage = (chat_id, token, message) => {
    console.log('chat_id', chat_id);
    console.log('token', token);
    console.log('message', message);
  return axios(`${URL}/api/v1/users/chat/${chat_id}`, {
    method: 'POST',
      headers: {
          'Authorization': `Bearer${token}`,
      },
      data: {message},
  }).then(({data}) => {
      console.log(NEW_CHAT_MESSAGE_SUCCESS, data);
    return {
      type: NEW_CHAT_MESSAGE_SUCCESS,
      data: {message, position: 'right'},
    };
  }).catch((error) => {
      console.log(NEW_CHAT_MESSAGE_ERROR, error);
      const {response} = error;
      console.log(`NEW_CHAT_MESSAGE_ERROR - response`, response);
      return {
          type: NEW_CHAT_MESSAGE_ERROR,
          message: response ? (response.data ? response.data.error.toString() : response.toString()) : response.toString(),
        };
  });
};

export const receivedChatMessage = async (message) => {
    return {
        type: NEW_CHAT_MESSAGE_SUCCESS,
        data: {message, position: 'left'},
    };
};
