import axios from 'axios';
import { FETCH_MATCHES_SUCCESS, CONFIRM_MATCHES_SUCCESS, CONFIRM_MATCHES_ERROR, FETCH_MATCHES_ERROR, URL } from '../constant';

export const fetchMatches = (token) => {
    // console.log('token', token);
  return axios(`${URL}/api/v1/matches`, {
    method: 'GET',
      headers: {
          'Authorization': `Bearer${token}`,
      },
  }).then(({data}) => {
    console.log('FETCH_MATCHES_SUCCESS', data);
      return {
      type: FETCH_MATCHES_SUCCESS,
      data,
    };
  }).catch(error => {
      console.log('FETCH_MATCHES_ERROR', error);
      return {
      type: FETCH_MATCHES_ERROR,
      message: error.toString(),
    };
  });
};

export const confirmMatch = (id, target_user_match, token) => {
  return axios(`${URL}/api/v1/matches/${id}`, {
    method: 'PATCH',
      headers: {
          'Authorization': `Bearer${token}`,
      },
      data: {target_user_match}
  }).then(({data}) => {
      console.log('data--->', data);
      if (data.status)
          return {
              type: CONFIRM_MATCHES_SUCCESS,
              data,
            };
  }).catch(message => {
      return {
      type: CONFIRM_MATCHES_ERROR,
      message: message.toString(),
    };
  });
};
