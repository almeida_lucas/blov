import axios from 'axios';
import {
    GOOGLE_API_KEY,
    FETCH_CITY_COORDS_ERROR,
    FETCH_CITY_COORDS_SUCCESS
} from '../constant';

export const fetchCoordsByCity = (city, state) => {
    console.log('fetchCoordsByCity', `https://maps.googleapis.com/maps/api/geocode/json?address=${city},${state}&key=${GOOGLE_API_KEY}`);
  return axios(`https://maps.googleapis.com/maps/api/geocode/json?address=${city},${state}&key=${GOOGLE_API_KEY}`, {
    method: 'GET',
  }).then(({data}) => {
    return {
      type: FETCH_CITY_COORDS_SUCCESS,
      data,
    };
  }).catch(message => {
    return {
      type: FETCH_CITY_COORDS_ERROR,
      message: message.toString(),
    };
  });
};
