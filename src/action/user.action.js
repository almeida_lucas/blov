import {AsyncStorage} from 'react-native';
import axios from 'axios';
import {
  DELETE_PICTURE_ERROR,
  DELETE_PICTURE_SUCCESS,
  FETCH_LOCAL_USER_ERROR,
  FETCH_LOCAL_USER_SUCCESS,
  LOGIN_ERROR,
  LOGIN_SUCCESS,
  REGISTER_ERROR,
  REGISTER_SUCCESS,
  UPDATE_USER,
  URL,
} from '../constant';

export const userLogin = (access_token, type_login, user_token) => {
  console.log('user_token', user_token)
  return axios(`${URL}/api/v1/users/auth`, {
    method: 'POST',
    data: {
      type_login,
      access_token,
      user_token,
    },
  }).then(({data}) => {
    console.log(LOGIN_SUCCESS, data);
    return {
      type: LOGIN_SUCCESS,
      data,
    };
  }).catch(message => {
    return {
      type: LOGIN_ERROR,
      message: message.toString(),
    };
  });
};

export const userRegister = async (user, token) => {
  return axios(`${URL}/api/v1/users/register`, {
    method: 'PUT',
    headers: {
      'Authorization': `Bearer${token}`,
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    data: user,
  }).then((resp) => {
    const {data} = resp;
    console.log('resp_userRegister', resp);
    console.log('data_userRegister', data);
    if (data.error) {
      return {
        type: REGISTER_ERROR,
        message: data.error,
      };
    }
    const newUser = {
      ...data,
      token,
    };
    const {pictures, ...rest} = newUser;
    return AsyncStorage.setItem('user', JSON.stringify(newUser))
    .then(response => {
      return {
        type: REGISTER_SUCCESS,
        data: newUser,
      };
    })
    .catch(error => {
      return {
        type: REGISTER_ERROR,
        error: error.toString(),
      };
    });
  }).catch(message => {
    console.log('message', message);
    return {
      type: REGISTER_ERROR,
      message: message.toString(),
    };
  });
};

export const fetchLocalUser = () => {
  return AsyncStorage.getItem('user')
  .then(userString => {
    return JSON.parse(userString);
  })
  .then(data => {
    return {
      type: FETCH_LOCAL_USER_SUCCESS,
      data,
    };
  })
  .catch(error => {
    return {
      type: FETCH_LOCAL_USER_ERROR,
      message: 'Usuário local não encontrado',
    };
  });
};

export const deletePicture = (id_usuario, id_foto, user) => {
  console.log('id_usuario-deletePicture', id_usuario);
  console.log('id_foto-deletePicture', id_foto);
  console.log('user-deletePicture', user);
  console.log('user.token-deletePicture', user.token);
  return axios(`${URL}/api/v1/users/${id_usuario}/pictures/${id_foto}`, {
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer${user.token}`,
    },
  }).then((resp) => {
    const {data} = resp;
    console.log('resp', resp);
    console.log('data', data);
    if (!data.status) {
      return {
        type: DELETE_PICTURE_ERROR,
        message: data.error,
      };
    }
    const pictures = user.pictures.filter(picture => picture.id !== id_foto);
    const newUser = {...user, ...pictures};
    return AsyncStorage.setItem('user', JSON.stringify(newUser))
    .then(response => {
      console.log('newUser_deletePicture', newUser);
      return {
        type: DELETE_PICTURE_SUCCESS,
        data: newUser,
      };
    })
    .catch(error => {
      return {
        type: DELETE_PICTURE_ERROR,
        error: error.toString(),
      };
    });
  }).catch(message => {
    console.log('message', message);
    return {
      type: DELETE_PICTURE_ERROR,
      message: message.toString(),
    };
  });
};

export const updateUserProp = async (data) => {
  return {
    type: UPDATE_USER,
    data,
  }
};
