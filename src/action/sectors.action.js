import axios from 'axios';
import {
    FETCH_SECTOR_LIST_ERROR,
    FETCH_SECTOR_LIST_SUCCESS,
    URL
} from '../constant';

export const fetchSectorList = () => {
  return axios(`${URL}/api/v1/sectors`, {
    method: 'GET',
  }).then(({data}) => {
    return {
      type: FETCH_SECTOR_LIST_SUCCESS,
      data,
    };
  }).catch(message => {
    return {
      type: FETCH_SECTOR_LIST_ERROR,
      message: message.toString(),
    };
  });
};
