/**
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';


type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={{width: 90, height: 90}}
          source={require('./src/resource/logo.png')}
        />

        <View style={{alignItems: 'center'}}>
          <Text style={{fontSize: 40, color: '#fff'}}>Olá <Text style={{fontWeight: 'bold'}}>Felipe</Text>,</Text>
          <Text style={{fontSize: 23, color: '#fff'}}>seja bem vindo</Text>
        </View>
        <Text style={{fontSize: 20, color: '#fff', textAlign: 'center'}}>
          Para começarmos, gostaríamos de saber um pouco mais sobre você, que tal?
        </Text>

        {/*TODO transformar em componente*/}
        <View style={{
          alignSelf: 'stretch',
          alignItems: 'stretch',
        }}>
          <TouchableOpacity onPress={() => {
          }}>
            <View style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: 50,
              borderWidth: 1,
              borderRadius: 5,
              borderColor: '#fff',
            }}>
              <Text style={{fontSize: 20, color: '#fff', textAlign: 'center'}}>Começar agora</Text>
            </View>
          </TouchableOpacity>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#7a1043',
    padding: 15,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
