/**
 * @format
 * @flow
 */

import { AsyncStorage } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { default as promise } from 'redux-promise';
import reducers from './src/reducer';
import InitialSignUp from './src/screen/initial.sign.up.screen';
import FirstSignUp from './src/screen/first.sign.up.screen';
import SecondSignUp from './src/screen/second.sign.up.screen';
import SearchScreen from './src/screen/search.screen';
import BlankScreen from './src/screen/blank.screen';
import SolicitScreen from './src/screen/solicit.screen';
import ChatItemScreen from './src/screen/chat.item.screen';
import ChatScreen from './src/screen/chat.screen';
import MyPlanScreen from './src/screen/my.plan.screen';
import LoginScreen from './src/screen/login.screen';
import Drawer from './src/component/drawer/drawer.component';
import HeaderComponent from './src/component/header.component';
import InitTabNavigation from './src/constant/init.tab.navigation';

const store = createStore(reducers, applyMiddleware(promise));

Navigation.registerComponentWithRedux(`InitialSignUp`, () => InitialSignUp, Provider, store);
Navigation.registerComponentWithRedux(`FirstSignUp`, () => FirstSignUp, Provider, store);
Navigation.registerComponentWithRedux(`SecondSignUp`, () => SecondSignUp, Provider, store);
Navigation.registerComponentWithRedux(`SearchScreen`, () => SearchScreen, Provider, store);
Navigation.registerComponentWithRedux(`LoginScreen`, () => LoginScreen, Provider, store);
Navigation.registerComponentWithRedux(`SolicitScreen`, () => SolicitScreen, Provider, store);
Navigation.registerComponentWithRedux(`ChatScreen`, () => ChatScreen, Provider, store);
Navigation.registerComponentWithRedux(`MyPlanScreen`, () => MyPlanScreen, Provider, store);
Navigation.registerComponent(`BlankScreen`, () => BlankScreen);
Navigation.registerComponentWithRedux(`ChatItemScreen`, () => ChatItemScreen, Provider, store);
Navigation.registerComponentWithRedux('Drawer', () => Drawer, Provider, store);
Navigation.registerComponentWithRedux('HeaderComponent', () => HeaderComponent, Provider, store);

Navigation.events().registerAppLaunchedListener(() => {
  AsyncStorage.getItem('user')
              .then(userString => {
                return JSON.parse(userString);
              })
              .then(data => {
                if (data)
                  InitTabNavigation();
                else
                  Navigation.setRoot({
                    root: {
                      stack: {
                        children: [
                          {
                            component: {
                              id: 'LoginScreen',
                              name: 'LoginScreen',
                            },
                          },
                        ],
                      },
                    },
                  });
              })
              .catch(error => {
                Navigation.setRoot({
                  root: {
                    stack: {
                      children: [
                        {
                          component: {
                            id: 'LoginScreen',
                            name: 'LoginScreen',
                          },
                        },
                      ],
                    },
                  },
                });
              });
});
